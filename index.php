<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>Aqua Panther</title>
        <meta name="viewport" content="width=device-width initial-scale=1 maximum-scale=1
            user-scalable=0 minimal-ui">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="HandheldFriendly" content="true" />
        <link rel="icon" href="http://phaser.io/images/img.png" type="image/x-icon" sizes="16x16">
<!--        <link rel="icon" href="//favicon.ico" type="image/x-icon" sizes="16x16">-->
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
        <![endif]-->
        <style type="text/css">
            *{
                margin: 0 auto;
                padding: 0;
            }
        </style>
        <script src="js/util/jquery.min.js"></script>
        <script src="js/phaser.min.js"></script>
        <script src="js/gameSetup.js"></script>
        <script src="js/boot.js"></script>
        <script src="js/load.js"></script>
        <script src="js/menu.js"></script>
        <script src="js/start_menu.js"></script>
        <script src="js/level_1.js"></script>
        <script src="js/level_1_2.js"></script>
        <script src="js/game.js"></script>

        <!-- Classes -->
        <script src="js/class/map.js"></script>
        <script src="js/class/player.js"></script>
        <script src="js/class/unit.js"></script>

        <!-- Components -->
        <script src="js/components/mapBody.js"></script>
        <script src="js/components/unitBody.js"></script>
    </head>
    <body>
    </body>
</html>