<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>Orange Panda</title>
        <link rel="stylesheet" href="css/qunit.css">
        <script src="js/util/jquery.min.js"></script>
        <script src="js/util/qunit.js"></script>
    </head>
    <body>
        <div id="qunit"></div>
        <div id="qunit-fixture">
            
        </div>
        <script src="js/util/jquery.min.js"></script>
        <script src="js/util/qunit.js"></script>
        <script src="js/gameSetup.js"></script>
        <script src="js/boot.js"></script>
        <script src="js/test.js"></script>
    </body>
</html>