var menuState = {
    create: function() {
        // Add a background image
        GameApp.game.add.image(0, 0, 'splash');
        // Display the name of the GameApp.game

        var text,
            scoreLabel,
            nameLabel = GameApp.game.add.text(400, -50, 'Project Aqua Panther',
            { font: '50px Arial', fill: '#FFF' });
        nameLabel.anchor.setTo(0.5, 0.5);
        //GameApp.game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        // If 'bestScore' is not defined
        // It means that this is the first time the GameApp.game is played
        if (!localStorage.getItem('bestScore')) {
            // Then set the best score to 0
            localStorage.setItem('bestScore', 0);
        }
        // If the score is higher than the best score
        if (GameApp.game.global.score > localStorage.getItem('bestScore')) {
            // Then update the best score
            localStorage.setItem('bestScore', GameApp.game.global.score);
        }    
        // Show the score at the center of the screen
        //text = 'score: ' + GameApp.game.global.score + '\nbest score: ' +
            //localStorage.getItem('bestScore');
        //scoreLabel = GameApp.game.add.text(GameApp.game.world.centerX, GameApp.game.world.centerY, text,
        //    { font: '25px Arial', fill: '#FFF', align: 'center' });
        //scoreLabel.anchor.setTo(0.5, 0.5);
        
        // Store the relevant text based on the device used
        if (GameApp.game.device.desktop) {
            text = 'press the up arrow key to start';
        }
        else {
            text = 'touch the screen to start';
        }
        // Display the text variable
        var startLabel = GameApp.game.add.text(GameApp.game.world.centerX, GameApp.game.world.height-80, text,
           { font: '25px Arial', fill: '#ffffff' });
            startLabel.anchor.setTo(0.5, 0.5); 
        
        GameApp.game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 500).loop()
                    .start();

        // Add the mute button that calls the 'toggleSound' function when pressed
        this.muteButton = GameApp.game.add.button(20, 20, 'mute', this.toggleSound, this);
        // If the mouse is over the button, it becomes a hand cursor
        this.muteButton.input.useHandCursor = true;
        
        // If the GameApp.game is already muted
        if (GameApp.game.sound.mute) {
            // Change the frame to display the speaker with no sound
            this.muteButton.frame = 1;
        }
        
        // Create a new Phaser keyboard variable: the up arrow key
        var upKey = GameApp.game.input.keyboard.addKey(Phaser.Keyboard.UP);
        // When the 'upKey' is pressed, it will call the 'start' function once
        upKey.onDown.addOnce(this.start, this);
        if (!GameApp.game.device.desktop) {

           GameApp.game.input.onDown.addOnce(this.start, this);
        }

    },
    start: function() {
        // Start the actual GameApp.game
        // GameApp.game.state.start('lvl_1_1');
        GameApp.game.state.start('start_manu');
    },
    // Function called when the 'muteButton' is pressed
    toggleSound: function() {
        // Switch the Phaser sound variable from true to false, or false to true
        // When 'GameApp.game.sound.mute = true', Phaser will mute the GameApp.game
        GameApp.game.sound.mute = ! GameApp.game.sound.mute;
        // Change the frame of the button
        this.muteButton.frame = GameApp.game.sound.mute ? 1 : 0;
    }
};