var StartmenuState = {
    create: function () {
        //Background screen
        this.mute = [];
        this.settingsSoundsText = [];
        this.startMenu = GameApp.game.add.sprite(150, 50, 'start_menu');
        //Single Line
        this.single = GameApp.game.add.text(GameApp.game.width / 2, GameApp.game.height - 450, 'Single(Coming soon)', {
            font: '30px Arial',
            fill: 'grey'
        });
        this.single.anchor.setTo(0.5, 0.5);

        //Multiplayer line
        this.multiplayer = GameApp.game.add.text(GameApp.game.width / 2, GameApp.game.height - 400, 'Multiplayer', {
            font: '30px Arial',
            fill: '#FFF'
        });
        this.multiplayer.anchor.setTo(0.5, 0.5);
        //Enable clicking and start game.
        console.log(this.multiplayer);
        this.multiplayer.inputEnabled = true;
        console.log(this.multiplayer);
        this.multiplayer.events.onInputDown.add(this.start);

        //Setting line
        this.settings_menu = GameApp.game.add.text(GameApp.game.width / 2, GameApp.game.height - 350, 'Settings', {
            font: '30px Arial',
            fill: '#FFF'
        });
        this.settings_menu.anchor.setTo(0.5, 0.5);
        //Enable clicking call show_settings function
        //To pass this of create add this to the function as parameter
        this.settings_menu.inputEnabled = true;
        this.settings_menu.events.onInputDown.add(function () {
            this.multiplayer.inputEnabled = false;
            this.settings_menu.inputEnabled = false;
            this.show_settings(this);
        }, this);

        console.log(this.multiplayer);
    },
    start: function () {
        // Start the actual GameApp.game
        // GameApp.game.state.start('lvl_1_1');
        GameApp.game.state.start('lvl_1_1');
    },

    show_settings: function (sprite) {
        this.blackScreen = GameApp.game.add.sprite(200, 100, 'start_menu');
        this.blackScreen.scale.setTo(0.8, 0.5);
// Add the mute button that calls the 'toggleSound' function when pressed
        StartmenuState.volume_buttons("All Sounds", 4, 220, 120);
        StartmenuState.volume_buttons("Game Sound", 1, 220, 150);
        StartmenuState.volume_buttons("Game Music", 2, 220, 180);
        StartmenuState.volume_buttons("Sound Effects", 3, 220, 210);
        this.backButton = GameApp.game.add.text(370, 300, "Back", {font: '30px Arial', fill: '#FFF'});
        this.backButton.inputEnabled = true;
        this.backButton.events.onInputDown.add(function () {
            console.log(sprite);
            //console.log(settings);
            for (i = 1; i <= 4; i++) {
                sprite.mute[i].kill();
                sprite.settingsSoundsText[i].kill();
            }
            sprite.backButton.kill();
            sprite.blackScreen.kill();
            sprite.multiplayer.inputEnabled = true;
            sprite.settings_menu.inputEnabled = true;
        }, {sprite: sprite});
   },

   volume_buttons: function (text, number, x, y) {
        this.settingsSoundsText[number] = GameApp.game.add.text(x, y, text, {font: '30px Arial', fill: '#FFF'});
        if (number == 4) {
            this.mute[number] = GameApp.game.add.button(x + 220, y + 5, 'mute', function () {
                if (this.mute[number].frame == 1) {
                    for (i = 1; i <= number; i++) {
                        this.mute[i].frame = 0;
                    }
                } else {
                    for (i = 1; i <= number; i++) {
                        this.mute[i].frame = 1;
                    }
                }
            }, this);
        } else {
            this.mute[number] = GameApp.game.add.button(x + 220, y + 5, 'mute', StartmenuState.toggleSound, self);
        }
        //// If the mouse is over the button, it becomes a hand cursor
        this.mute[number].input.useHandCursor = true;
        //
        //// If the GameApp.game is already muted
        if (GameApp.game.sound.mute) {
            // Change the frame to display the speaker with no sound
            this.mute[number].frame = 1;
        }

    },
    // Function called when the 'muteButton' is pressed
    toggleSound: function (sprite) {
        // Switch the Phaser sound variable from true to false, or false to true
        // When 'GameApp.game.sound.mute = true', Phaser will mute the GameApp.game
        GameApp.game.sound.mute = !GameApp.game.sound.mute;
        // Change the frame of the button
        sprite.frame = GameApp.game.sound.mute ? 1 : 0;
    }
};