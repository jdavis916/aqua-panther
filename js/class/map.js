// Create the Map entity
Map = function(game, rowNum, colNum, state) {
    // treat this as create function
    //Phaser.Sprite.call(this, game, x, y, sprite);

    this.game = game;
    this.rowNum = rowNum;
    this.colNum = colNum;
    this.state = state;
    this.components = {};
    this.side1Grid = [1];
    this.side2Grid = [2];

    /*console.log('global-grid-x : ', GameApp.game.global);
    console.log('global-grid-y : ', GameApp.game.global.map.props.gridSpaceY);
    console.log("[Map] :: create");
    console.log('row : ', rowNum, ' : cols : ', colNum);*/
    // Add Components
    var MapBody = this.addComponent(new Components.MapBody());
    //

    //var Breakable = this.addComponent(new Components.Breakable());

    // This is the GLUE!
    //MapBody.onCreate.add(Breakable.break, this);
    function onStart() {

        //	Turn off the delay, so it loops seamlessly from here on
        this.delay(0);

    }


    //game.add.existing(this);
    this.createMap();
};

Map.prototype = Object.create(Phaser.Sprite.prototype);
Map.prototype.constructor = Map;


// Method to add components
Map.prototype.addComponent = function(comp) {
    this.components[comp.name] = comp;
    this.components[comp.name].setTarget(this);
    //this.input.enableDrag(true);
    console.log("[Map] :: added component " + comp.name);
    return comp;
};

Map.prototype.getComponent = function(componentName) {
    return this.components[componentName];
};

Map.prototype.update = function() {
    //console.log("[Map] :: update");
    Object.keys(this.components).forEach(function(a, b, c) {
        this.components[a].update();
    }, this);
};
Map.prototype.render = function() {
    //console.log("[Map] :: render");

    var i,
        x,
        y;
        //grid = this.getMap(),
        //len = grid.length;

    //for(i = 0; i < len; ++i) {
    //    for (x = 0; x < this.rowNum; ++x) {
    //        for (y = 0; y < this.colNum; ++y) {
    //            this.game.debug.geom(grid[i][1][1][x][y], 'rgba(255,0,0,1)');
    //        }
    //    }
    //}
    //this.game.debug.spriteInfo(this, 400, 100);
    // call components render function
    Object.keys(this.components).forEach(function(a, b, c) {
        this.components[a].render();
    }, this);
};
Map.prototype.createMap = function() {
    var self = this; // cache entity context


    console.log("[Map] :: createMap");

    //add grid to each side
    this.side1Grid.push(createGrid(this.side1Grid[0]));
    this.side2Grid.push(createGrid(this.side2Grid[0]));
    this.getMap();
    //console.log(GameApp.game.global.grid);
    function createGrid(sideNum){
        var mySide = [],
            rect,
            rectGrid = [],
            myGrid = [],
            x,
            y;

        //for(x = 0; x < GameApp.game.global.map.rows; ++x)
        //{
        //    //create grid for tracking
        //    //myGrid.push(new Array(self.colNum));
        //    myGrid.push([]);
        //    mySide = [];
        //
        //    for(y = 0; y < GameApp.game.global.map.cols; ++y)
        //    {
        //        // sets X coordinates of map
        //        if(y === 0 && sideNum === 1){
        //            GameApp.game.global['map']['coords']['coordX'].push((x * GameApp.game.global.map.props.gridSpaceX) + GameApp.game.global.map.props.sideStart);
        //        }
        //
        //        //sets Y coordinates of map for both sides
        //        if(x === 0 && sideNum === 1) {
        //            GameApp.game.global['map']['coords']['coordY1'].push(GameApp.game.global.map.props.side1FrontX + y * 30);
        //        }else if (x === 0 && sideNum === 2) {
        //            GameApp.game.global['map']['coords']['coordY2'].push(GameApp.game.global.map.props.side2FrontX - y * 30);
        //        }
        //
        //        //console.log('Y : ', y * 30);
        //
        //        //add rectangles for each column
        //        if(sideNum === 1)
        //            rect = new Phaser.Rectangle((x * GameApp.game.global.map.props.gridSpaceX) + GameApp.game.global.map.props.sideStart , (GameApp.game.global.map.props.side1FrontX + y * 30), 20, 20);
        //        else
        //            rect = new Phaser.Rectangle((x * GameApp.game.global.map.props.gridSpaceX) + GameApp.game.global.map.props.sideStart , (GameApp.game.global.map.props.side2FrontX - y * 30), 20, 20);
        //        myGrid[x].push({"x" : rect.x, "y" : rect.y});
        //        mySide.push(rect);
        //    }
        //
        //
        //
        //    rectGrid.push(mySide);
        //}

        for(y = 0; y < GameApp.game.global.map.rows; ++y)
        {
            //create grid for tracking
            //myGrid.push(new Array(self.colNum));
            myGrid.push([]);
            mySide = [];

            for(x = 0; x < GameApp.game.global.map.cols; ++x)
            {
                // sets X coordinates of map
                if(y === 0 && sideNum === 1){
                    GameApp.game.global['map']['coords']['coordX'].push(((x * GameApp.game.global.map.props.gridSpaceX) +
                    GameApp.game.global.map.props.sideStart));
                }

                //sets Y coordinates of map for both sides
                if(x === 0 && sideNum === 1) {
                    GameApp.game.global['map']['coords']['coordY1'].push((GameApp.game.global.map.props.side1FrontX + y * GameApp.game.global.map.props.gridSpaceY));
                    console.log('-------X-----------');
                    console.log(GameApp.game.global['map']['coords']['coordY1']);
                }else if (x === 0 && sideNum === 2) {
                    GameApp.game.global['map']['coords']['coordY2'].push((GameApp.game.global.map.props.side2FrontX - y * GameApp.game.global.map.props.gridSpaceY) );
                }

                //add rectangles for each column
                if(sideNum === 1)
                    rect = new Phaser.Rectangle(((x * GameApp.game.global.map.props.gridSpaceX) +
                    GameApp.game.global.map.props.sideStart), (GameApp.game.global.map.props.side1FrontX + y *
                    GameApp.game.global.map.props.gridSpaceY), 20, 20);
                else
                    rect = new Phaser.Rectangle(((x * GameApp.game.global.map.props.gridSpaceX) +
                    GameApp.game.global.map.props.sideStart), (GameApp.game.global.map.props.side2FrontX - y *
                    GameApp.game.global.map.props.gridSpaceY), 20, 20);

                myGrid[y].push({"x" : rect.x, "y" : rect.y, "occupied" : 0, "accessible" : 1});
                console.log(rect);
                mySide.push(rect);
                console.log(mySide);
            }



            //rectGrid.push(mySide);
        }







        //console.log('my grid : ', myGrid);
        if(sideNum === 1)
            console.log('map coords : ', GameApp.game.global['map']['coords']);

        return [
            //returns [tracking_grid, rectangle_grid]
            myGrid,
            rectGrid
        ];
    }
};
Map.prototype.getMap = function() {
    // each array sets [side_number, [tracking_grid, rectangle_grid]]
    GameApp.game.global['grid']['side1'] = this.side1Grid;
    GameApp.game.global['grid']['side2'] = this.side2Grid;
    console.log('-------GET MAP GRID------');
    console.log(GameApp.game.global['grid']['side1']);
    console.log(GameApp.game.global['grid']['side2']);
    //return [
    //    // each array returns [side_number, [tracking_grid, rectangle_grid]]
    //    this.side1Grid,
    //    this.side2Grid
    //]
};
