// Create the Unit entity
Unit = function(game, x, y, id, unitSide, color, unitType, width, length, sprite, state, timer_delay) {
    // treat this as create function
    var spriteUnit;
    this.game = game;
    this.state = state;
    this.delay = timer_delay * 1000 + 2000;
    this.id = id;
    this.unitSide = unitSide;
    this.unitPosX = x;  // X coordinates
    this.unitPosY = y; // Y coordinates
    //this.unitStopY = move;
    this.sideBegin = this.game.height + 100;
    //this.unitJSON = this.game.cache.getJSON('units');
    //console.log(sprite);
    //console.log(this.unitJSON[sprite]['state']['idle']['image']);
    //this.spriteImage = this.unitJSON[sprite]['state']['idle']['image'];
    if(this.unitSide === 2){
        this.sideBegin = -100;
    }

    //spriteUnit = this.game.add.sprite( x, y, this.spriteImage);
    //Phaser.Sprite.call(this, this.game, this.unitPosX , this.sideBegin, 'ship_1_idle');


    if(this.unitSide === 2){
        //this.anchor.setTo(0, 1);
        Phaser.Sprite.call(this, this.game, this.unitPosX , this.sideBegin, 'ship_1_1_2');
    } else {
        Phaser.Sprite.call(this, this.game, this.unitPosX , this.sideBegin, 'ship_1_1_1');
    }
    this.anchor.setTo(.5,.5);

    //  Input Enable the sprites


    if(this.unitSide === 2){
        this.game.add.tween(this).to( { angle: 180 }, 0, Phaser.Easing.Linear.None, true);
    }



    console.log(this);
    //  Input Enable the spri tes
    this.inputEnabled = true;
    this.input.enableSnap(50, 30, true, true);

    this.game.input.addMoveCallback(this.updateMarker, this);

    //this.events.onInputUp.add(this.onDown, this);
    this.events.onInputDown.add(this.onDown, this);
    // Limit sprite movement to grid.
    //this.events.onDragStop.add(this.restrictMovement);
    this.events.onDragStop.add(this.dragStop);

   /* console.log(this);
    console.log('move : ', move);*/

    // move to final position
    this.game.add.tween(this).to( { y: this.unitPosY }, this.delay, "Quart.easeOut", true, 1000, 0);

    /*console.log('num : ', this.unitSide);
    console.log('start-y : ', this.y);
    console.log('side : ', GameApp.game.global['map']['coords']['coordY' + this.unitSide] );*/
    this.dropSuccessful = false;
    this.width = GameApp.game.global.map.props.gridSpaceX; // sets width, overwrites scale.x
    this.height = GameApp.game.global.map.props.gridSpaceY;  // sets width, overwrites scale.y
    this.unitStartPosX = GameApp.game.global.map.coords.coordX.indexOf(this.unitPosX) + 1; // X grid coordinates (start)
    this.unitStartPosY = GameApp.game.global.map.coords['coordY' + this.unitSide].indexOf(this.unitPosY) + 1 ; // Y grid coordinates (start)

    this.unitEndPosX = (GameApp.game.global['map']['coords']['coordX'].indexOf(this.unitPosX) + 1); // Y grid coordinates (end)
    this.unitEndPosY = (GameApp.game.global['map']['coords']['coordY' + this.unitSide].indexOf(this.unitPosY) + 1); // Y grid coordinates (end)
    this.unitSelected = 0;
    //this.anchor.setTo(0.5, 0.5);
    //this.animations.add('idle', [1, 2, 3, 4, 5, 6, 7, 8, 9], 30, true);
    //this.spriteUnit = spriteUnit;
    //this.animations.add('idle', [0, 1, 2, 3, 4, 5, 4, 3, 2, 1]);
    this.animations.add('idle', [0]);
    //console.log(this.animation.add('idle'));
    
        //if(this.spriteImage = this.unitJSON[sprite]['state']['idle']['anim'] === 'yes'){
        // Create the 'right' animation by looping the frames 1 and 2
        //console.log(this);

        //}

    ////	There is a 2.5 second delay at the start, then it calls this function
    //this.onStart.add(onStart, this);
    ////  Allow dragging - the 'true' parameter will make the sprite snap to the center

    this.components = {};
    //console.log(sprite);
    //this.scale.setTo(.1,.1);
    //console.log("[Unit] :: create");
    this.props = {
        'color' : color,
        'type' : unitType,
        'currentPos' : [this.unitPosX, this.unitPosY]

    };
    //console.log('unit-id : ', this.id);
    // Add Components
    var UnitBody = this.addComponent(new Components.UnitBody(this.props));
    // Health Component
    // Battle Component

    //var Breakable = this.addComponent(new Components.Breakable());

    // This is the GLUE!
    //UnitBody.onCreate.add(Breakable.break, this);

    this.game.add.existing(this);
    /*if(this.unitJSON[sprite]['state']['idle']['anim'] === 'yes'){
        this.play('idle', 6, true);
    }*/
};

Unit.prototype = Object.create(Phaser.Sprite.prototype);
Unit.prototype.constructor = Unit;


// Method to add components
Unit.prototype.addComponent = function(comp) {
    this.components[comp.name] = comp;
    this.components[comp.name].setTarget(this);
    this.input.enableDrag(true);
    //console.log("[Unit] :: added component " + comp.name);
    return comp;
};

Unit.prototype.getComponent = function(componentName) {
    return this.components[componentName];
};

Unit.prototype.update = function() {
    //console.log("[Unit] :: update");

    Object.keys(this.components).forEach(function(a, b, c) {
        this.components[a].update();
    }, this);

};
// fires when mouse is clicked anywhere
Unit.prototype.onMouseDown = function() {
    GameApp.game.global['side1']['unitSelected'] = false;
    //GameApp.game.global['side2']['unitSelected'] = false;
    GameApp.game.global['side1']['selectedID'] = 0;
    //GameApp.game.global['side2']['selectedID'] = 0;
    //this.onSelectUpdate();
};

// fires when mouse clicked on this unit
Unit.prototype.onDown = function() {
    var i,
        colClear = true;
    console.log('----------------------onDown Fired ---------------------');

    //console.log(GameApp.game.global.map.marker['p' + this.unitSide].y);
    //console.log(this.y);
    //GameApp.game.global.map.marker.p1.x
    //GameApp.game.global.map.marker['p' + this.unitSide].y

    // call drop function i this unit is already selected
    if(this.unitSelected === 1){
        console.log('----------------------drop Fired ---------------------');
        this.dropUnit();
    }
    // only continue if player marker is in the same place as the ship
    if(this.y !== GameApp.game.global.map.marker['p' + this.unitSide].y){
        return false;
    }
    /*console.log('above : ', this.unitStartPosY, ' ', GameApp.game.global.map.rows);
    console.log(this.unitPosX);
    console.log(this.unitPosY);*/

    /*this.props.currentPos = [this.unitStartPosX, this.unitStartPosY];*/
    /*console.log('x : ', this.unitStartPosX);
    console.log('y : ', this.unitStartPosY);*/
    /*if(this.unitEndPosX > GameApp.game.global['map']['coords']['coordX'][this.mapColsCount]){
        this.unitEndPosX = GameApp.game.global['map']['coords']['coordX'][this.mapColsCount];
    }*/
    console.log(this.unitStartPosY);
    console.log(typeof this.unitStartPosY)/*;*/

    for(i = this.unitStartPosY; i < GameApp.game.global.map.rows; ++i){
        if(GameApp.game.global['grid']['side' + this.unitSide][1][0][i][this.unitStartPosX-1].occupied !== 0){
            colClear = false;
            this.input.draggable = false;
            break;
        }
    }
    /*console.log('Col Clear : ', colClear);
    console.log('----------');
    console.log(this.input.draggable);*/
    if (colClear){
        this.input.draggable = true;
        //this.unitSelected = 1;
        console.log('-----------Y POS ----------------');
        GameApp.game.global.updates.player['p' + this.unitSide].unitID = this.id;
        console.log(GameApp.game.global.map.controlRows['row' + this.unitSide]);
        this.y = GameApp.game.global.map.controlRows['row' + this.unitSide];
        console.log(this.y );
        this.input.allowVerticalDrag = false;
    }

    //console.log("TURNS PLAYER MADE: " + GameApp.game.global.updates.player['p1']['turns']);
    /*if(GameApp.game.global.updates.player['p1'].turns > 14 ){
        this.input.draggable = false;
    }*/

   this.onSelect();
    //this.setUpdateUnit();
    //console.log('unit id : ', this.id);
    //console.log('selected id : ', this.id);
    //console.log('x: ', this.x);
    //console.log('y: ', this.y);
    //console.log(GameApp.game.global['map']['coords']['coordX'].indexOf(this.x));
    //console.log('unit-row: ', this.unitStartPosX);
    //console.log('unit-col: ', this.unitStartPosY);
};
Unit.prototype.setUnit = function() {
    //var grid = GameApp.game.global.grid;
    //console.log(grid.side1);
    //console.log(GameApp.game.global['map']['coords']['coordX']);
    //console.log(GameApp.game.global['map']['coords']['coordY' + this.unitSide]);
    //console.log(this.x);
    //console.log(this.y);
    //console.log(this.unitStartPosX);
    //console.log(this.unitStartPosY);



    //this.unitStartPosX = (GameApp.game.global['map']['coords']['coordX'].indexOf(this.x) + 1);
    //this.unitStartPosY = (GameApp.game.global['map']['coords']['coordY' + this.unitSide].indexOf(this.unitStopY) + 1);




    //console.log(this.unitStartPosX);
    //console.log(this.unitStartPosY);
    //console.log(this.unitStartPosY);
    //console.log(GameApp.game.global.grid['side1']);
    //console.log(GameApp.game.global.grid['side' + this.unitSide][1][0]);
    //console.log(GameApp.game.global.grid['side' + this.unitSide][1][0][this.unitStartPosY-1]);
    //console.log(GameApp.game.global.grid['side' + this.unitSide][1][0][this.unitStartPosY-1][this.unitStartPosX-1]);
    //console.log(parseFloat(this.unitStartPosX));
    //console.log(parseFloat(this.unitStartPosY));
    //console.log(this.unitStartPosX);
    //console.log(this.unitStartPosY);
    //GameApp.game.global.grid['side' + this.unitSide][1][0][this.unitStartPosY-1][this.unitStartPosX-1].occupied = 1;
    console.log(GameApp.game.global.grid['side' + this.unitSide][1][0]);
    //GameApp.game.global.grid['side' + this.unitSide][1][0][this.unitStartPosY-1][this.unitStartPosX-1].props = this.props;

    //this.unitStartPosX = parseFloat(this.unitStartPosX);
    //this.unitStartPosY = parseFloat(this.unitStartPosY);
};
Unit.prototype.setUpdateUnit = function() {

    //if(this.x >= GameApp.game.global.map.props.sideEnd) {
    //    this.unitEndPosX = GameApp.game.global.map.cols;
    //}else if(this.x < GameApp.game.global.map.props.sideEnd){
    //    this.unitEndPosX = 1;
    //}else{
    //    this.unitEndPosX = (GameApp.game.global['map']['coords']['coordX'].indexOf(this.x) + 1);
    //
    //}
    //if(this.y < GameApp.game.global.map.props.side1FrontX){
    //    this.unitEndPosY =  GameApp.game.global.map.rows;
    //}else if(this.y >= GameApp.game.global.map.props.side1CapitalFront){
    //    this.unitEndPosY = 8;
    //}else{
    //    this.unitEndPosY = (GameApp.game.global['map']['coords']['coordY' + this.unitSide].indexOf(this.y) + 1);
    //}
    /*console.log('---------------before--------------------------');*/
   /* console.log(this.unitStartPosX);
    console.log(this.unitStartPosY);
    console.log(this.unitEndPosX);
    console.log(this.unitEndPosY);
    console.log(GameApp.game.global.grid.side1);*/
    // Update grid objects
    this.updateGrid();

    this.unitStartPosX = parseFloat(this.unitEndPosX);
    this.unitStartPosY = parseFloat(this.unitEndPosY);



   // this.unitStartPosX = (GameApp.game.global['map']['coords']['coordX'].indexOf(this.x) + 1);
   // this.unitStartPosY = (GameApp.game.global['map']['coords']['coordY' + this.unitSide].indexOf(this.y) + 1);


    /*console.log('---------------after--------------------------');
    console.log(this.unitStartEndX);
    console.log(this.unitStartEndY);
    console.log(this.unitStartEndX);
    console.log(this.unitStartEndY);*/
};
Unit.prototype.selectUnit = function() {
    //GameApp.game.global['side1']['unitSelected'] = true;
    //GameApp.game.global['side1']['selectedID'] = this.id;
    //this.setUnit();
    //console.log('found-pos-x : ', this.x);
    //console.log('found-pos-y : ', this.y);
    //console.log('found-x : ', this.unitStartPosX);
    //console.log('found-y : ', this.unitStartPosY);
    //this.onSelectUpdate();
};
Unit.prototype.onSelect = function() {
    //GameApp.game.global['side1']['unitSelected'] = true;
    //GameApp.game.global['side1']['selectedID'] = this.id;
    //this.selectUnit();
    //console.log('found-x : ', this.unitStartPosX);
    this.onSelectUpdate();
};
Unit.prototype.onSelectUpdate = function() {
    //var matches = this.unitMatch();
    /*console.log('--------------');
    console.log(this);*/
    GameApp.game.global['updates']['unit']['selected'] = true;
    GameApp.game.global['updates']['unit']['id'] = this.id ;
    GameApp.game.global['updates']['unit']['x'] = this.unitStartPosX;
    GameApp.game.global['updates']['unit']['y'] = this.unitStartPosY;
    GameApp.game.global['updates']['unit'].type = this.props.type;
    GameApp.game.global['updates']['unit'].color = this.props.color;
    //GameApp.game.global['updates']['unit']['matchingHorizontal'] = matches.h;
    //GameApp.game.global['updates']['unit']['matchingVertical'] = matches.v;
    GameApp.game.global['updates']['unit']['cellX'] = true;
    GameApp.game.global['updates']['unit']['cellY'] = true;

};
Unit.prototype.updateGrid = function() {
    var i = 0,
        j = 0,
        len = GameApp.game.global['side' + this.unitSide]['units'].length,
        sideUnits = GameApp.game.global['side' + this.unitSide]['units'],
        matches;
    console.log(sideUnits);
    console.log(GameApp.game.global.grid['side' + this.unitSide][1][0]);
    console.log('------------units-----------------');
    console.log(GameApp.game.global['side' + this.unitSide]['units']);

    // reset occupied status i grid objects
    GameApp.game.global.map.actions.resetOccupy(this.unitSide);

    /*
    console.log(GameApp.game.global['side' + this.unitSide]['units']);
    console.log('-------------------------------------');
    console.log(GameApp.game.global['grid']['side1']);
    console.log(GameApp.game.global['grid']['side2']);*/
    console.log('START');

    console.log(sideUnits[i].unitStartPosX);
    console.log(sideUnits[i].unitStartPosY);
    console.log('END');
    console.log(sideUnits[i].unitEndPosX);
    console.log(sideUnits[i].unitEndPosY);


    console.log(GameApp.game.global.grid['side' + this.unitSide][1][0]);
    // Set all grid objects with units to occupied
    for(; i < len; ++i){

        /*console.log('----IN LOOP');
        console.log('START');

        console.log(sideUnits[i].unitStartPosX);
        console.log(sideUnits[i].unitStartPosY);
        console.log('END');
        console.log(sideUnits[i].unitEndPosX);
        console.log(sideUnits[i].unitEndPosY);
        console.log(i);
        console.log(GameApp.game.global.grid['side' + this.unitSide][1][0][sideUnits[i].unitEndPosY - 1][sideUnits[i].unitEndPosX - 1]);*/
        GameApp.game.global.grid['side' + this.unitSide][1][0][sideUnits[i].unitEndPosY - 1][sideUnits[i].unitEndPosX - 1].occupied = 1;
    }

    console.log(GameApp.game.global.grid['side' + this.unitSide][1][0]);
    //matches = this.unitMatch();
};
Unit.prototype.dropUnit = function(){
    var i = 0,
        unitSpaceX = GameApp.game.global.map.coords.coordX.indexOf(this.x);
    console.log('------DROP UNIT HIT-----------');
    console.log('unit-X : ', unitSpaceX);
    for(; i < GameApp.game.global.map.rows; ++i){
        if(GameApp.game.global['grid']['side' + this.unitSide][1][0][i][unitSpaceX].occupied === 0){
            this.dropSuccessful = true;
            //this.input.draggable = false;
            this.game.add.tween(this).to( { y: GameApp.game.global.map.coords['coordY' + this.unitSide][i] }, 200, "Quart.easeOut", true);

            break;
        }
    }
    if(this.dropSuccessful){
        this.unitSelected = 0;
        GameApp.game.global.updates.player['p' + this.unitSide].unitID  = 0;
        this.dropSuccessful = false;
    }else{
        console.log('column not open');
    }

};
Unit.prototype.updateMarker = function(){

    //GameApp.game.global.map.marker.p1.x = GameApp.game.global.map.layer.currentLayer.getTileX(GameApp.game.input.activePointer.worldX) * 32;
    //GameApp.game.global.map.marker.p1.y = GameApp.game.global.map.layer.currentLayer.getTileY(GameApp.game.input.activePointer.worldY) * 32;
    //console.log(GameApp.game.input.activePointer.worldX);
    //console.log(GameApp.game.input.activePointer.worldY);

    //GameApp.game.global.map.marker.p1.x = GameApp.game.global.map.layer.currentLayer.getTileX(GameApp.game.input.activePointer.worldX);
    //GameApp.game.global.map.marker.p1.y = GameApp.game.global.map.layer.currentLayer.getTileY(GameApp.game.input.activePointer.worldY);


    //console.log(this.id);
    //console.log(GameApp.game.global.map.marker.p1);
    //console.log(GameApp.game.global.map.marker.p1.x);
    //console.log(GameApp.game.global.map.marker.p1.y );
    //console.log(GameApp.game.global.map.layer.currentLayer.getTileX(GameApp.game.input.activePointer.worldX));
    //if(this.input.pointerOver()){
        //GameApp.game.global.map.marker.p1


        /*this.tileMap.putTile(this.currentTile, GameApp.game.global.map.layer.currentLayer.
            getTileX(GameApp.game.global.map.marker.p1.x), GameApp.game.global.map.layer.currentLayer.
            getTileY(GameApp.game.global.map.marker.p1.y), GameApp.game.global.map.layer.currentLayer);*/
    //}



    //if (GameApp.game.input.mousePointer.isDown)
    //{

        // map.fill(currentTile, currentLayer.getTileX(marker.x), currentLayer.getTileY(marker.y), 4, 4, currentLayer);
    //}
};
Unit.prototype.dragStop = function(sprite) {
    var testPosX,
        testPosY;

    if(this.unitSelected !== 1){


    } else{


        if (sprite.x < GameApp.game.global.map.props.sideStart) {
            sprite.x = GameApp.game.global.map.props.sideStart;
        }
        else if (sprite.x > GameApp.game.global.map.props.sideEnd - GameApp.game.global.map.props.gridSpaceX) {
            sprite.x = GameApp.game.global.map.props.sideEnd - GameApp.game.global.map.props.gridSpaceX;
        }
        // constrains the sprite to side 1
        if (sprite.unitSide === 1) {
            if (sprite.y < GameApp.game.global.map.props.side1FrontX) {
                sprite.y = +GameApp.game.global.map.props.side1FrontX;
            }
            else if (sprite.y >= GameApp.game.global.map.props.side1CapitalFront) {
                sprite.y = (GameApp.game.global.map.props.side1CapitalFront - GameApp.game.global.map.props.gridSpaceY);
            }
        }
    //    Create code for player 2
    }
    testPosX = GameApp.game.global['map']['coords']['coordX'].indexOf(sprite.x) + 1;
    testPosY = GameApp.game.global['map']['coords']['coordY' + sprite.unitSide].indexOf(sprite.y) + 1;
    //console.log('first : ',GameApp.game.global.map.props.sideEnd);
    console.log(testPosX);
    console.log(testPosY);

    console.log('---------dragStop Fired ----------');

    //skips this function if unit didnt move
    if(sprite.unitStartPosX === testPosX && sprite.unitStartPosY === testPosY){
        return false;
    }
    console.log('---------dragStop Fired 2----------');

    console.log('---------POSITION X / Y ----------');

   /* console.log(sprite.x);
    console.log(sprite.y);
    console.log('-------------------');
    console.log(GameApp.game.global['map']['coords']['coordX']);
    console.log(GameApp.game.global['map']['coords']['coordY' + sprite.unitSide]);*/

    // sets up end position (X)
    if(sprite.x >= GameApp.game.global.map.props.sideEnd) {
        sprite.unitEndPosX = GameApp.game.global.map.cols;
    }else if(sprite.x < GameApp.game.global.map.props.sideStart){
        sprite.unitEndPosX = 1;
    }else{
        sprite.unitEndPosX = testPosX;

    }
    // sets up end position (Y)
    if(sprite.y < GameApp.game.global.map.props.side1FrontX){
        sprite.unitEndPosY =  1;
        //sprite.y = GameApp.game.global['map']['coords']['coordY' + sprite.unitSide][0];
    }else if(sprite.y >= GameApp.game.global.map.props.side1CapitalFront){
        sprite.unitEndPosY = GameApp.game.global.map.rows;
        //sprite.y = GameApp.game.global['map']['coords']['coordY' + sprite.unitSide][GameApp.game.global.map.rows - 1];
    }else{
        sprite.unitEndPosY = testPosY;
    }

    //sprite.unitEndPosY = (GameApp.game.global['map']['coords']['coordY' + sprite.unitSide].indexOf(sprite.y) + 1);


    /*console.log('---------------STARTING - X/Y----------------');
    console.log(sprite.unitStartPosX);
    console.log(sprite.unitStartPosY);
    console.log('---------------X/Y----------------');
    console.log(sprite.unitEndPosX);
    console.log(sprite.unitEndPosY);*/
    sprite.setUpdateUnit();
    /*if(sprite.unitStartPosX !== sprite.unitEndPosX || sprite.unitStartPosY !== sprite.unitEndPosY) {

    }*/

    //if(sprite.unitEndPosX > GameApp.game.global['map']['coords']['coordX'][this.mapColsCount - 1]){
    //    sprite.unitEndPosX = GameApp.game.global['map']['coords']['coordX'][this.mapColsCount - 1];
    //}
    //console.log(sprite.unitEndPosX);
    //sprite.unitStartPosX = +sprite.unitEndPosX;
    //sprite.unitStartPosY = +sprite.unitEndPosY;

    //sprite.unitStartPosX = parseFloat(sprite.unitEndPosX);
    //sprite.unitStartPosY = parseFloat(sprite.unitEndPosY);


    //GameApp.game.global.updates.player['p1'].turns++;
};
Unit.prototype.restrictMovement = function(sprite) {

    // constrains the horizontal movement of the sprite to the game grid
    if (sprite.x < GameApp.game.global.map.props.sideStart) {
        sprite.x = GameApp.game.global.map.props.sideStart;
    }
    else if (sprite.x > GameApp.game.global.map.props.sideEnd - GameApp.game.global.map.props.gridSpaceX) {
        sprite.x = GameApp.game.global.map.props.sideEnd - GameApp.game.global.map.props.gridSpaceX;
    }
    // constrains the sprite to side 1
    if(sprite.unitSide === 1){
        if (sprite.y < GameApp.game.global.map.props.side1FrontX) {
            sprite.y = +GameApp.game.global.map.props.side1FrontX;
        }
        else if (sprite.y >= GameApp.game.global.map.props.side1CapitalFront) {
            sprite.y = (GameApp.game.global.map.props.side1CapitalFront - GameApp.game.global.map.props.gridSpaceY);
        }
    }
    // Create for player 2

    //sprite.onSelect();
};
Unit.prototype.unitMatch = function() {
    var matches = {
        h : 0,
        v : 0
    };
    console.log(this.unitStartPosX);
    console.log(this.unitStartPosY);

    //console.log(GameApp.game.global.grid.side1[1][0]);
    //console.log(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-2]);

    // match horizontally

    // check left side
    if (this.unitStartPosX > 1) {
        if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-2].occupied === 1){
            console.log('inside');
            if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.color === GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-2].props.color
                //&& GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.type === GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-2].props.type
            ){
                ++matches.h;
            }
        }
    }

    // check right side
    if (this.unitStartPosX < GameApp.game.global.map.cols) {

        if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX].occupied === 1){
            if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.color === GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX].props.color
                //&& GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.type === GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX].props.type
            ){
                ++matches.h;
            }
        }
    }

    // match vertically

    // check front side
    if (this.unitStartPosY > 1) {
        if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-2][this.unitStartPosX-1].occupied === 1){
            if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.color === GameApp.game.global.grid.side1[1][0][this.unitStartPosY-2][this.unitStartPosX-1].props.color
                //&& GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.type === GameApp.game.global.grid.side1[1][0][this.unitStartPosY-2][this.unitStartPosX-1].props.type
            ){
                ++matches.v;
            }
        }
    }

    // check back side
    if (this.unitStartPosX < GameApp.game.global.map.rows) {
        if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY][this.unitStartPosX-1].occupied === 1){
            if(GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.color === GameApp.game.global.grid.side1[1][0][this.unitStartPosY][this.unitStartPosX-1].props.color
                //&& GameApp.game.global.grid.side1[1][0][this.unitStartPosY-1][this.unitStartPosX-1].props.type === GameApp.game.global.grid.side1[1][0][this.unitStartPosY][this.unitStartPosX-1].props.type
            ){
                ++matches.v;
            }
        }
    }
    return matches;
};