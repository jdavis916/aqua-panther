// Create the Player entity
Player = function(game, x, y, id, playerSide, PlayerPic) {
    // treat this as create function

    this.game = game;
    this.id = id;
    this.playerSide = playerSide ;
    this.playerPosX = x;
    this.playerPosY = y;
    this.PlayerPic = PlayerPic;
    //this.PlayerJSON = this.game.cache.getJSON('Players');
    //console.log(sprite);
    //console.log(this.PlayerJSON[sprite]['state']['idle']['image']);
    //this.spriteImage = this.PlayerJSON[sprite]['state']['idle']['image'];
    //spritePlayer = this.game.add.sprite( x, y, this.spriteImage);
    var player = GameApp.game.add.sprite(this.playerPosX, this.playerPosY, this.PlayerPic);
    player.scale.setTo(0.4, 0.4);

    if(this.playerSide === 2){
        player.anchor.setTo( .5, .5);
        //player.scale.x = -0.4;
    }




    this.bmd = this.game.add.bitmapData(200, 20);
    this.bmd.ctx.beginPath();
    this.bmd.ctx.rect(0, 0, 500, 80);
    this.bmd.ctx.fillStyle = '#009900';
    this.bmd.ctx.fill();

    this.widthLife = new Phaser.Rectangle(0, 0, 200, this.bmd.height);
    this.totalLife = this.bmd.width;

    if(this.playerSide > 1){
        this.life = this.game.add.sprite(this.playerPosX+ 80, this.playerPosY+ 75, this.bmd);
        this.life.scale.x = -1;
    } else {
        this.life = this.game.add.sprite(this.playerPosX, this.playerPosY + 140, this.bmd);
    }
    this.life.anchor.y = 0.5;
    this.life.cropEnabled = true;
    this.game.time.events.loop(1000, this.cropLife, this);

};

Player.prototype = Object.create(Phaser.Sprite.prototype);
Player.prototype.constructor = Player;

Player.prototype.cropLife = function(){
    if(this.widthLife.width <= 0){
        this.widthLife.width = this.totalLife;
    }
    else{
        this.game.add.tween(this.widthLife).to( { width: (this.widthLife.width - (this.totalLife / 10)) }, 200, Phaser.Easing.Bounce.Out, true);
    }
    //console.log(this.widthLife);
    this.life.crop(this.widthLife);

};
// Method to add components
Player.prototype.addComponent = function(comp) {
    this.components[comp.name] = comp;
    this.components[comp.name].setTarget(this);
    this.input.enableDrag(true);
    //console.log("[Player] :: added component " + comp.name);
    return comp;
};

Player.prototype.getComponent = function(componentName) {
    return this.components[componentName];
};

Player.prototype.update = function() {
    //console.log("[Player] :: update");



};