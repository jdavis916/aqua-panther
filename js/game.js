// Initialise Phaser
GameApp.game = new Phaser.Game(800, 600, Phaser.AUTO, 'gameDiv');

// Define our 'global' variable
GameApp.game.global = {
    grid: {
        side1 : {},
        side2 : {}
    },
    map: {
        bounds : {
            x : null,
            y : null
        },
        coords : {
            coordX : [],
            coordY1 : [],
            coordY2 : []
        },
        currentLevel : {
            rows : 0,
            cols : 0
        },

        layer : {
            bg : null,
            grid : null,
            marker : null,
            unit : null,
            unitAnim : null,
            mapAnim : null,
            ui : null,
            menu : null
        },
        layerNames : ['bg', 'grid', 'marker', 'unit', 'unitAnim', 'mapAnim', 'ui', 'menu'],
        marker : {
            p1 : null,
            p2 : null,
            p3 : null,
            p4 : null
        },
        actions : {
            resetOccupy : function(unitSide){
                var i = 0,
                    j = 0;

                for(; i < GameApp.game.global.map.rows; ++i){
                    j = 0;
                    for(; j < GameApp.game.global.map.cols; ++j){
                        GameApp.game.global.grid['side' + unitSide][1][0][i][j].occupied = 0;
                    }
                }
            }
        },
        spawns :{
            'spawn1' : GameApp.game.height + 100,
            'spawn2' : -100
        },
        controlRows :{
            'row1' : GameApp.game.height - 50,
            'row2' : 100
        },
        props : {}
    },
    menu : {
        pause : {
           'x1' : 200,
           'x2' : 600,
           'y1' : 50,
           'y2' : 550
        }
    },
    score: 0,
    side1 : {
        selectedID : 0,
        unitSelected : false,
        row : 0,
        col : 0,
        units: []
    },
    side2 : {
        selectedID : 0,
        unitSelected : false,
        row : 0,
        col : 0,
        units: []
    },
    updates : {
        unit : {
            id : 0,
            selected : false,
            side : 0,
            x : 0,
            y : 0,
            cellX : 0,
            cellY : 0,
            type : null,
            color : null,
            matchingHorizontal : 0,
            matchingVertical : 0
        },
        player : {
            'p1' : {
                'turns' : 0,
                'unitID': 1
            },
            'p2' : {
                'turns' : 0,
                'unitID': 1
            }
        }
    },
    utils : {
        setMap : function(){
            var sideStart  = GameApp.game.width * 0.25,
                sideEnd = GameApp.game.width * 0.75,
                side1FrontX = GameApp.game.height * 0.60,
                side2FrontX = GameApp.game.height * 0.40,
                sideWidth = sideEnd - sideStart,
                sideHeight = GameApp.game.height * 0.40,
                sideFieldHeight = sideHeight * 0.75,
                side1CapitalFront = side1FrontX + sideFieldHeight,
                sideCapitalHeight = sideHeight * 0.2,
                gridSpaceX = sideWidth / GameApp.game.global.map.cols,
                gridSpaceY = sideFieldHeight / GameApp.game.global.map.rows;
                sideStart += gridSpaceX / 2;
                side1FrontX += gridSpaceY / 2;
                side2FrontX += gridSpaceY / 2;
            return {
                gameHeight: GameApp.game.height,
                gameWidth: GameApp.game.width,
                sideStart: sideStart,
                sideEnd: sideEnd,
                side1FrontX: side1FrontX,
                side2FrontX: side2FrontX,
                sideWidth: sideWidth,
                sideHeight: sideHeight,
                sideFieldHeight: sideFieldHeight,
                side1CapitalFront: side1CapitalFront,
                sideCapitalHeight: sideCapitalHeight,
                gridSpaceX: gridSpaceX,
                gridSpaceY: gridSpaceY
            }
        },
        getMap : function(){
            GameApp.game.global.map.props = GameApp.game.global.utils.setMap();
            //accepts x pos number
            GameApp.game.global.map.bounds.x = function(n){
                return !!(n > GameApp.game.global.map.props.sideStart && n < GameApp.game.global.map.props.sideEnd);
            } ;

            //accepts y pos number and side of field
            //later add condition for side 2 !!important
            GameApp.game.global.map.bounds.y = function(n, s){
                if(s === 1){
                    return !!(n > GameApp.game.global.map.props['side'+s+'FrontX'] && n < GameApp.game.global.map.props['side'+s+'CapitalFront']);
                }else{
                    /*console.log('--------------GLOBAL ------------------');
                    console.log(n);
                    console.log('grid-y : ', GameApp.game.global.map.props.gridSpaceY);
                    console.log(GameApp.game.global.map.props['sideCapitalHeight'] + GameApp.game.global.map.props.gridSpaceY );
                    console.log(GameApp.game.global.map.props['side2FrontX']);
                    console.log(GameApp.game.global.map.props['sideCapitalHeight']);*/
                    return !!(n > GameApp.game.global.map.props['sideCapitalHeight'] + (GameApp.game.global.map.props.gridSpaceY * 2)&& n < GameApp.game.global.map.props['side'+s+'FrontX'] +
                    GameApp.game.global.map.props.gridSpaceY);
                }
            } ;
            console.log(GameApp.game.global.map.props);

        },
        getOpenSquares : function(unitSide){
            var openArr = [],
                i= 0,
                j = 0,
                len1 = GameApp.game.global.map.rows,
                len2 = GameApp.game.global.map.cols;


            /* console.log('-----------HIT-----------------------');
                console.log(len1);
                console.log(len2);
            */
            //console.log('-----------HIT-----------------------');


            for(; i < len1; ++i) {
                j = 0;
                //console.log(i);
                //console.log('-----------------');

                for (; j < len2; ++j) {
                    console.log(GameApp.game.global.grid['side' + unitSide][1][0][i][j]);
                    //console.log('------------J----------------');
                    //console.log(j);
                    //console.log(GameApp.game.global.grid['side' + unitSide][1][0][i][j].accessible);
                    //console.log(GameApp.game.global.grid['side' + unitSide][1][0][i][j].occupied);


                    //console.log(len2);
                    if (GameApp.game.global.grid['side' + unitSide][1][0][i][j].occupied === 0 &&
                        GameApp.game.global.grid['side' + unitSide][1][0][i][j].accessible === 1) {

                        openArr.push(GameApp.game.global.grid['side' + unitSide][1][0][i][j]);
                    }
                }
            }
            console.log('-----------HIT5-----------------------');

            console.log(openArr);
            console.log(openArr.length);
             /* console.log('-----------HIT6-----------------------');
*/
            return openArr;
        }
    }
};

// Add all the states
GameApp.game.state.add('boot', bootState);
GameApp.game.state.add('load', loadState);
GameApp.game.state.add('menu', menuState);
GameApp.game.state.add('start_manu', StartmenuState);
//GameApp.game.state.add('play', playState);
// Chapter 1
GameApp.game.state.add('lvl_1_1', GameApp.Level);
GameApp.game.state.add('lvl_1_2', GameApp.Level_1_2);
// game.state.add('lvl_1_2', GameApp.Level);
// game.state.add('lvl_1_3', GameApp.Level);
// game.state.add('lvl_1_4', GameApp.Level);
// game.state.add('lvl_1_5', GameApp.Level);

// Start the 'boot' state
GameApp.game.state.start('boot');