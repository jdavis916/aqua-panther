

var oneState = {

    preload: function() {
        game.load.image('player', 'assets/player2.png');
        game.load.image('enemy', 'assets/enemy.svg');
        game.load.image('wallH', 'assets/wall_3.svg');
        game.load.image('wallV', 'assets/wall_4.svg');
        game.load.image('star', 'assets/star.png');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        
        this.player = game.add.sprite(game.world.centerX - 10, game.world.centerY + 57, 'player');
        this.player.anchor.setTo(0.5, 0.5);
        // game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;

        // Create an enemy group with Arcade physics
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        // Create 10 enemies with the 'enemy' image in the group
        // The enemies are "dead" by default, so they are not visible in the game
        this.enemies.createMultiple(10, 'enemy');
        
        // Call 'addEnemy' every 2.2 seconds
        game.time.events.loop(2200, this.addEnemy, this);

        // Display the coin
        this.star = game.add.sprite(60, 148, 'star');
        // Add Arcade physics to the coin
        game.physics.arcade.enable(this.star);
        // Set the anchor point of the coin to its center
        this.star.anchor.setTo(0.5, 0.5);

        this.cursor = game.input.keyboard.createCursorKeys();
        this.createWorld();
        // Display the score
        this.scoreLabel = game.add.text(30, 30, 'score: 0',
        { font: '18px Arial', fill: '#ffffff' });
        // Initialise the score variable
        this.score = 0;
    },
    update: function() {
        // this.sprite.angle += 1;
        
        // Tell Phaser that the player and the walls should collide
        game.physics.arcade.collide(this.player, this.walls);
        this.movePlayer();
        game.physics.arcade.overlap(this.player, this.star, this.takeCoin, null, this);
        if (!this.player.inWorld) 
        {
            this.playerDie();
        }
        // Make the enemies and walls collide
        game.physics.arcade.collide(this.enemies, this.walls);
        // Call the 'playerDie' function when the player and an enemy overlap
        game.physics.arcade.overlap(this.player, this.enemies, this.playerDie,
        null, this);
    },
    createWorld: function() {
        // Create our wall group with Arcade physics
        this.walls = game.add.group();
        this.walls.enableBody = true;
        // Create the 10 walls
        game.add.sprite(0, 0, 'wallV', 0, this.walls); // Left
        game.add.sprite(480, 0, 'wallV', 0, this.walls); // Right
        game.add.sprite(-180, 0, 'wallH', 0, this.walls); // Top left
        game.add.sprite(320, 0, 'wallH', 0, this.walls); // Top right
        game.add.sprite(-120, 320, 'wallH', 0, this.walls); // Bottom left
        game.add.sprite(300, 320, 'wallH', 0, this.walls); // Bottom right
        game.add.sprite(-240, 160, 'wallH', 0, this.walls); // Middle left
        game.add.sprite(400, 160, 'wallH', 0, this.walls); // Middle right
        
        var middleTop = game.add.sprite(90, 80, 'wallH', 0, this.walls);
        middleTop.scale.setTo(1, 1);
        var middleBottom = game.add.sprite(90, 240, 'wallH', 0, this.walls);
        middleBottom.scale.setTo(1, 1);
        // Set all the walls to be immovable
        this.walls.setAll('body.immovable', true);
    },
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
        }
        else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = 200;
        }
        else {
            this.player.body.velocity.x = 0;
        }
        if (this.cursor.up.isDown && this.player.body.touching.down) { 
            this.player.body.velocity.y = -320;
        }
    },
    playerDie: function() {
        game.state.start('main');
    },
    addEnemy: function() {
        // Get the first dead enemy of the group
        var enemy = this.enemies.getFirstDead();
        // If there isn't any dead enemy, do nothing
        if (!enemy) {
            return;
        }
        // Initialise the enemy
        enemy.anchor.setTo(0.5, 1);
        enemy.reset(game.world.centerX, 0);
        enemy.body.gravity.y = 500;
        enemy.body.velocity.x = 100 * Phaser.Math.randomSign();
        enemy.body.bounce.x = 1;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    },
    takeCoin: function(player, coin) {
        // Increase the score by 5
        this.score += 5;
        // Update the score label
        this.scoreLabel.text = 'score: ' + this.score;
        // Change the coin position
        this.updateCoinPosition();
    },
    updateCoinPosition: function() {
        // Store all the possible coin positions in an array
        var coinPosition = [
        {x: 140, y: 60}, {x: 360, y: 60}, // Top row
        {x: 60, y: 140}, {x: 440, y: 140}, // Middle row
        {x: 130, y: 300}, {x: 370, y: 300} // Bottom row
        ];
        // Remove the current coin position from the array
        // Otherwise the coin could appear at the same spot twice in a row
        for (var i = 0; i < coinPosition.length; i++) {
        if (coinPosition[i].x === this.star.x) {
        coinPosition.splice(i, 1);
        }
        }
        // Randomly select a position from the array
        var newPosition = coinPosition[
        game.rnd.integerInRange(0, coinPosition.length-1)];
        // Set the new position of the coin
        this.star.reset(newPosition.x, newPosition.y);
    }
}
    // We initialising Phaser
    var game = new Phaser.Game(500, 340, Phaser.AUTO, 'gameDiv');

    game.state.add('main', mainState);
    game.state.start('main');
    