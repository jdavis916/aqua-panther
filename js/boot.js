var bootState = {
    preload: function () {
        // Load the image
        GameApp.game.load.image('progressBar', 'assets/progressBar.png');
        //GameApp.game.load.image('ninja', 'assets/ninja.png');
    },
    create: function() {
        // Set some GameApp.game settings
        GameApp.game.stage.backgroundColor = 'background';
        GameApp.game.physics.startSystem(Phaser.Physics.ARCADE);
        console.log(GameApp.game.scale);
        // If the device is not a desktop, so it's a mobile device
        if (!GameApp.game.device.desktop) {
            // Set the type of scaling to 'show all'
            GameApp.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            // Add a blue color to the page, to hide the white borders we might have
            document.body.style.backgroundColor = '#3498db';
            // Set the min and max width/height of the GameApp.game
            GameApp.game.scale.minWidth = 250;
            GameApp.game.scale.minHeight = 170;
            GameApp.game.scale.maxWidth = 1000;
            GameApp.game.scale.maxHeight = 680;
            // Center the GameApp.game on the screen
            GameApp.game.scale.pageAlignHorizontally = true;
            GameApp.game.scale.pageAlignVertically = true;
            // Apply the scale changes

        }
        // Start the load state
        GameApp.game.state.start('load');
    }
};
    