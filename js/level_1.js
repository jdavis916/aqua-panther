GameApp.Level = function() {
	this.game = GameApp.game;
    this.tileMap = null;
    this.screenLayers = [];
    this.offScreen = -1000;
    this.c = 'rgb(255,255,255)';
    //this.currentLayer = null;
    this.currentTile = null;
    this.marker = null;
    this.marker2 = null;
    this.counter = 0;
    this.counter2 = 0;
    this.pauseArea = null;
    this.continueGame_menu = null;
    this.settings_menu = null;
    this.choiceLabel = null;
    this.mapRowsCount = GameApp.game.global.map.rows = 6;
    this.mapColsCount = GameApp.game.global.map.cols = 8;
    this.map = null;
    this.side1 = null;
    this.side1Field = null;
    this.side1Capital = null;
    this.side2 = null;
    this.side1FrontX = 0;
    this.side2FrontX = 0;
    this.side2Field = null;
    this.side2Capital = null;
    this.gridColSide1 = [];
    this.gridRowSide1 = [];
    this.gridColSide2 = [];
    this.gridRowSide2 = [];
    this.side1Units = [];
    this.side2Units = [];
    this.maxUnits = 20;
    this.sideStart = 0;
    //this.gridSpace = 50;
    this.gridSpaceX = 0;
    this.gridSpaceY = 0;
    this.offsetStartY1 = 100;
    this.offsetStartY2 = 400;
    this.offsetEndX1 = 100;
    this.offsetEndX2 = 600;
    this.offsetMapX = 100;
    this.offsetMapY = 100;
    this.mainMap = [];
    this.enemySide = [];
    this.mySide = [];
    this.unitID = 1;
    this.unitLabel = null;
  //this.line1;
};

GameApp.Level.prototype = {

	preload: function() {
		// this.game.load.image('sky', 'assets/sky.png');
  //   	this.game.load.spritesheet('player', 'assets/player2.png', 20, 20);
  //       this.game.load.image('enemy', 'assets/enemy.png');
  //       this.game.load.image('coin', 'assets/coin.png');
  //       this.game.load.image('door', 'assets/door.png');
  //       this.game.load.image('tileset', 'assets/tileset.png');
  //       this.game.load.tilemap('map', 'assets/map3.json', null, Phaser.Tilemap.TILED_JSON);
  //          this.game.load.image('unit_test', 'assets/ninja.png');
  //          this.game.load.image('unit_samurai', 'assets/samurai.jpg');
  //          this.game.load.image('ship', 'assets/Ship.png');
  //      this.game.load.image('menu', 'assets/number-buttons-90x90.png', 270, 180);

	},

	create: function() 
	{
		// enable physics arcade system
	    this.game.physics.startSystem(Phaser.Physics.ARCADE);
        GameApp.game.add.sprite(0,0, 'background');

        // Create the background layer, behind everything and does not move.
        //var bgLayer = this.game.add.group();
        //this.screenLayers[0] = bgLayer;
        //this.screenLayers[0].z = 0;
        //// Create the cloud layer, only beyond the sky.
        //var mapLayer = this.game.add.group();
        //this.screenLayers[1] = mapLayer;
        //this.screenLayers[1].z = 1;
        //// Create the ground, behind the river and beyond clouds.
        //var unitLayer = this.game.add.group();
        //this.screenLayers[2] = unitLayer;
        //this.screenLayers[2].z = 15;
        this.createLayers();


        GameApp.game.global.utils.getMap();

        this.unitLabel = this.game.add.text(50, 15, 'Unit Selected : ' + GameApp.game.global['updates']['unit']['selected'], { font: '20px Arial', fill: '#FFF' });
        this.unitLabel2 = this.game.add.text(50, 55, 'Unit ID : ' + GameApp.game.global['updates']['unit']['id'], { font: '20px Arial', fill: '#FFF' });
        this.unitLabel3 = this.game.add.text(350, 15, 'Current X : ' + GameApp.game.global['updates']['unit']['x'], { font: '20px Arial', fill: '#FFF' });
        this.unitLabel4 = this.game.add.text(350, 55, 'Current Col : ' + GameApp.game.global['updates']['unit']['y'], { font: '20px Arial', fill: '#FFF' });
        this.unitLabel5 = this.game.add.text(600, 15, 'Current Type : ' + GameApp.game.global['updates']['unit']['x'], { font: '20px Arial', fill: '#FFF' });
        this.unitLabel6 = this.game.add.text(600, 55, 'Current Color : ' + GameApp.game.global['updates']['unit']['y'], { font: '20px Arial', fill: '#FFF' });
        this.unitLabel7 = this.game.add.text(600, 95, 'Matching-V : ' + GameApp.game.global['updates']['unit']['x'], { font: '20px Arial', fill: '#FFF' });
        this.unitLabel8 = this.game.add.text(600, 135, 'Matching-H : ' + GameApp.game.global['updates']['unit']['y'], { font: '20px Arial', fill: '#FFF' });

        // this.createWorld();
        this.game.input.onDown.add(this.onMouseDown, this);
        //  Creates a blank tilemap
        this.tileMap = this.game.add.tilemap();
        this.createMap();
        this.createPlayer(1, 0, 400, "captain_1_1_1");
        this.createPlayer(2, 730, 100,"captain_2_1_1");


        //this.spawnUnits(1);
        this.spawnUnits(1, 15);
        this.spawnUnits(2, 15);
        //this.setUnits();
        // Create a label to use as a button
        this.pause_label = this.game.add.text(700, 550, 'Pause', { font: '24px Arial', fill: '#FFF' });
        this.pause_label.inputEnabled = true;
        this.pause_label.events.onInputUp.add(this.pauseMenu);


        // Add a input listener that can help us return from being paused
        this.game.input.onDown.add(this.resumeGame, self);

        // create marker for grid ( player 1 )
        this.marker = this.game.add.graphics();
        this.marker.lineStyle(2, 0x00FF00, 1);
        this.marker.beginFill(0x00FF00, 1);
        this.marker.drawRect(0, 0, GameApp.game.global.map.props.gridSpaceX, GameApp.game.global.map.props.gridSpaceY);

        // create marker for grid ( player 2 )
        this.marker2 = this.game.add.graphics();
        this.marker2.lineStyle(2, 0xF00F00, 1);
        this.marker2.beginFill(0xF00F00, 1);
        this.marker2.drawRect(0, 0, GameApp.game.global.map.props.gridSpaceX, GameApp.game.global.map.props.gridSpaceY);

        // add marker to marker layer
        GameApp.game.global.map.layer.marker.add(this.marker);
        GameApp.game.global.map.layer.marker.add(this.marker2);

        // save marker in global
        GameApp.game.global.map.marker.p1 = this.marker;
        GameApp.game.global.map.marker.p1.x = -200;

        GameApp.game.global.map.marker.p2 = this.marker2;
        GameApp.game.global.map.marker.p2.x = -200;
        console.log(GameApp.game.global.map.marker.p2);
        //this.layer2 = this.tileMap.createBlankLayer('level2', 40, 30, 32, 32);
        //this.layer2.scrollFactorX = 0.8;
        //this.layer2.scrollFactorY = 0.8;

        //this.currentLayer = this.layer2;
        //GameApp.game.global.map.layer.currentLayer = this.currentLayer;
        //this.game.input.addMoveCallback(this.updateMarker, this);
	},

	update: function() {
        //console.log("TURNS PLAYER MADE: " + GameApp.game.global.updates.player['p1']['turns']);
        var i = 2;
		// this.game.physics.arcade.collide(this.stars, this.platforms);
	 	this.game.physics.arcade.TILE_BIAS = 40;
        //console.log('Unit Selected : ', GameApp.game.global['updates']['unit']['selected']);
        this.unitLabel.setText('Unit Selected :  ' + GameApp.game.global['updates']['unit']['selected']);
        if(GameApp.game.global['updates']['unit']['id'] > 0){
            this.unitLabel2.setText('Unit ID : ' + GameApp.game.global['updates']['unit']['id']);
            this.unitLabel3.setText('Current X : ' + GameApp.game.global['updates']['unit']['x']);
            this.unitLabel4.setText('Current Y : ' + GameApp.game.global['updates']['unit']['y']);
            this.unitLabel5.setText('Current Type : ' + GameApp.game.global['updates']['unit'].type);
            this.unitLabel6.setText('Current Color : ' + GameApp.game.global['updates']['unit'].color);
            this.unitLabel7.setText('Match - H : ' + GameApp.game.global['updates']['unit']['matchingHorizontal']);
            this.unitLabel8.setText('Match - V : ' + GameApp.game.global['updates']['unit']['matchingVertical']);

        }else{
            // reset all labels
            for(;i<9; ++i){
                this['unitLabel'+i].setText('');
            }
        }

        this.updateMarker();


	},
    paused: function(){

    console.log('Game Paused');

    },
	render: function() {
        //call rendder function from entities
        this.map.render();

        //this.game.debug.geom(this.line1, this.c);
        //this.game.debug.geom(this.side2Field,'#333');
        //this.game.debug.geom(this.side2Capital,'#CCC');
        //this.game.debug.cameraInfo(this.game.camera, 32, 32);


        //this.game.debug.geom(this.side1,'#0F0');

        //this.game.debug.geom(this.side1Field,'#333');

        //this.game.debug.geom(this.side1Capital,'#CCC');
        //this.game.debug.geom(this.side2,'#00F');

        //for(x = 0; x <= this.mapRowsCount; ++x)
        //{
        //    this.game.debug.geom(this.gridRowSide1[x], this.c);
        //}
        //for(x = 0; x <= this.mapRowsCount; ++x)
        //{
        //    this.game.debug.geom(this.gridRowSide2[x], this.c);
        //}
        //
        //for(x = 0; x <= this.mapColsCount; ++x)
        //{
        //    this.game.debug.geom(this.gridColSide2[x], this.c);
        //}


	},
    createWorld: function() {
        // Create the tilemap
        // this.map = this.game.add.tilemap('map');
        // Add the tileset to the map
        // this.map.addTilesetImage('tileset');
        // Create the layer, by specifying the name of the Tiled layer
        // this.layer = this.map.createLayer('Ground');
        // Set the world size to match the size of the layer
        // this.layer.resizeWorld();
        // Enable collisions for the first element of our tileset (the blue wall)
        // this.map.setCollision(1);
    },

    createMap: function() {
        // create new map
        this.map = new Map(this.game, this.mapRowsCount, this.mapColsCount, null);
    },
    createPlayer: function(side, x, y, captainName) {
        this.player = new Player(this.game, x, y, 1, side, captainName );

    },
    createLayers: function(){
        var i = 0,
            temp = null,
            len = GameApp.game.global.map.layerNames.length;
        console.log(GameApp.game.global.map.layer);
        for(; i < len; ++i){
            /*console.log(GameApp.game.global.map.layerNames[i]);
            console.log(GameApp.game.global.map.layer['bg']);
            console.log(GameApp.game.global.map.layer[GameApp.game.global.map.layerNames[i]]);*/

                GameApp.game.global.map.layer[GameApp.game.global.map.layerNames[i]] = GameApp.game.add.group();
            //GameApp.game.global.map.layer[GameApp.game.global.map.layerNames[i]].z = i;
        }
        console.log(GameApp.game.global.map.layer)
    },
    spawnUnits: function (side, freeUnits) {
        var i,
            j,
            unit,
            rand_unit,
            unitSide = side, // which side is calling spawn
            unitAmount = freeUnits, // amount of units in pool
            currentRow = null,
            currentCol = null,
            //sprite = ['ship_1','ship_2' ],
            sprite = ['ship_1'],
            rand,
            color,
            unitType,
            unitObj,
            mapUnitsArr = [[], [], [], [], [], []],
            openArr = GameApp.game.global.utils.getOpenSquares(unitSide),
            len = openArr.length,
            len2 = mapUnitsArr.length,
            len3 = 0;

        for(i = 0; i < unitAmount; ++i){

            rand = Math.floor(Math.random() * (len - i));
            //console.log(unitAmount - i);
            unitObj = openArr.splice(rand, 1);
            console.log(unitObj);
            unitObj = unitObj[0];
            console.log(unitObj.x);
            console.log(unitObj.y, ' <--this one');
            console.log(GameApp.game.global.map.coords['coordX']);
            console.log(GameApp.game.global.map.coords['coordY' + unitSide], ' <--this one');
            mapRow = GameApp.game.global.map.coords['coordY' + unitSide].indexOf(unitObj.y);
            console.log(GameApp.game.global.map.coords['coordY' + unitSide].indexOf(unitObj.y), ' <--this one');

            mapUnitsArr[mapRow].push(unitObj);
        }
        /*console.log('-------MAP UNITS ARR----------------');
        console.log(mapUnitsArr);

        console.log('-------OPEN ARR----------------');
        console.log(openArr);
        console.log(openArr.length);*/

        /*currentCol = i % this.mapColsCount;
        color = this.game.rnd.integerInRange(1, 3);
        unitType = this.game.rnd.integerInRange(1, 3);*/
        /*console.log(len2);*/
        for(i = 0; i < len2; ++i){
            len3 = mapUnitsArr[i].length;
            /*console.log(len3);*/
            for(j = 0; j < len3; ++j){
                /*console.log(mapUnitsArr[i][j]);*/
                this.spawnUnit(mapUnitsArr[i][j], side);
            }
        }
        //console.log('----------------FINAL COUNTER---------------');
        //console.log(this.counter);
        //
        //console.log('----------------FINAL COUNTER 2---------------');
        //console.log(this.counter2);


        //return false;
        /*for(i = 0; i <  this.maxUnits; ++i) {
            rand_unit = sprite[Math.floor(Math.random() * sprite.length)];

            currentCol = i % this.mapColsCount;
            color = this.game.rnd.integerInRange(1, 3);
            unitType = this.game.rnd.integerInRange(1, 3);


            //if (rand > 3) {
                // Spanws Unit
                unit = new Unit(this.game,
                    GameApp.game.global.map.props.sideStart + (GameApp.game.global.map.props.gridSpaceX * currentCol), // x
                    (GameApp.game.height + 100), // y
                    this.unitID++, // id
                    unitSide, // player side
                    color, // color
                    unitType, // type
                    GameApp.game.global.map.props.gridSpaceX, // width
                    GameApp.game.global.map.props.gridSpaceY,  // length
                    rand_unit, // sprite
                    null, // state
                    GameApp.game.global.map.props.side1FrontX + (GameApp.game.global.map.props.gridSpaceY * currentRow), // move
                    currentRow // timer delay
                );
                //unit.animations.add('idle', [1, 2, 3, 4, 5, 6, 7, 8, 9], 30, true);
                //unit.play('idle');
                //console.log(unit);
                //this.screenLayers[2][].push(unit);
                GameApp.game.global.map.layer.unit.add(unit);
                this.side1Units.push(unit);
            }
            else {
                console.log("skip");
            }
        }*/
            //if (rand > 5) {
            //unit = new Unit(this.game, this.sideStart + (this.gridSpaceX * currentCol), this.side1FrontX + (this.gridSpaceY * currentRow));
            //
            //    this.screenLayers[2].add(unit);
            //    this.side1Units.push(unit);
            //} else {
            //    console.log("skip");
            //}

        /*console.log(this.side1Units[0]);*/

         // add units to global list
         GameApp.game.global['side' + unitSide]['units'] = this['side' + unitSide + 'Units'];

    },
    setUnits: function() {
        /*console.log(GameApp.game.global.grid);
        console.log(GameApp.game.global.grid['side1']);*/
        this.side1Units.forEach(function(self){
           //console.log(self);
            self.setUnit();
        });
        this.side2Units.forEach(function(self){
            //console.log(self);
            self.setUnit();
        });
    },
    spawnUnit: function(mapObj, side) {
        var i = 0,
            unitX = GameApp.game.global.map.coords.coordX.indexOf(mapObj.x),
            unitY = GameApp.game.global.map.coords['coordY' + side].indexOf(mapObj.y);
        /*console.log('----INSIDE SPAWN UNIT------');
        console.log(GameApp.game.global.map.rows);*/
            //console.log(mapObj);
            //console.log(unitX);
            //console.log(typeof GameApp.game.global['grid']['side' + side][1][0][0][unitX].occupied);
            for(; i < GameApp.game.global.map.rows-1; ++i){
                if(GameApp.game.global['grid']['side' + side][1][0][i][unitX].occupied === 0){
                    this.createUnit(unitX, i, side);
                    GameApp.game.global['grid']['side' + side][1][0][i][unitX].occupied = 1;
                    break;
                }else{
                    GameApp.game.global['grid']['side' + side][1][0][i][unitX].occupied = 1;
                }
            }
    },
    createUnit: function(unitX, unitY, side){
        var unit,
            unitSide = side, // which side is calling spawn
            currentRow = null,
            sprite = ['ship_1'],
            rand_unit = sprite[Math.floor(Math.random() * sprite.length)],
            color= this.game.rnd.integerInRange(1, 3),
            unitType = this.game.rnd.integerInRange(1, 3);

        /* console.log('-----CREATE INDIVIDUAL UNIT----------');
       console.log(unitX);
        console.log(unitY);
        console.log(GameApp.game.global.map.coords.coordX[unitX]);
        console.log(GameApp.game.global.map.coords['coordY' + side][unitY]);*/
        unit = new Unit(this.game,
            GameApp.game.global.map.coords.coordX[unitX], // x
            GameApp.game.global.map.coords['coordY' + side][unitY], // y
            GameApp.game.global.updates.player['p' + side].unitID++, // id
            unitSide, // player side
            color, // color
            unitType, // type
            GameApp.game.global.map.props.gridSpaceX, // width
            GameApp.game.global.map.props.gridSpaceY,  // length
            rand_unit, // sprite
            null, // state
            currentRow // timer delay
        );

        //console.log('-----UNIT-ID---------');
        //console.log(GameApp.game.global.updates.player['p' + side].unitID);
        GameApp.game.global.map.layer.unit.add(unit);
        this['side' + side+ 'Units'].push(unit);
        // add units to global list
        GameApp.game.global['side' + unitSide]['units'] = this.side1Units;
    },
    //createTileSelector: function () {
    //
    //    //  Our tile selection window
    //    this.tileSelector = GameApp.game.add.group();
    //
    //    this.tileSelectorBackground = GameApp.game.make.graphics();
    //    this.tileSelectorBackground.beginFill(0x000000, 0.5);
    //    this.tileSelectorBackground.drawRect(0, 0, 800, 34);
    //    this.tileSelectorBackground.endFill();
    //
    //    this.tileSelector.add(tileSelectorBackground);
    //
    //    this.tileStrip = tileSelector.create(1, 1, 'ground_1x1');
    //    this.tileStrip.inputEnabled = true;
    //    this.tileStrip.events.onInputDown.add(this.pickTile, this);
    //
    //    this.tileSelector.fixedToCamera = true;
    //
    //    //  Our painting marker
    //    //this.marker = GameApp.game.add.graphics();
    //    //this.marker.lineStyle(2, 0x000000, 1);
    //    //this.marker.drawRect(0, 0, 32, 32);
    //
    //},
    //pickTile: function (sprite, pointer) {
    //
    //    this.currentTile = game.math.snapToFloor(pointer.x, 32) / 32;
    //
    //},
    updateMarker: function(){
        if(GameApp.game.input.mousePointer.y > GameApp.game.height / 2) {
            console.log('----PLAYER 1 UPDATE------------');
            if (GameApp.game.global.map.bounds.x(GameApp.game.input.x) && GameApp.game.global.map.bounds.y(GameApp.game.input.y, 1)) {
                GameApp.game.global.map.marker.p1.x = (~~(GameApp.game.input.x / GameApp.game.global.map.props.gridSpaceX) * GameApp.game.global.map.props.gridSpaceX);
                GameApp.game.global.map.marker.p1.y = (~~(GameApp.game.input.y / GameApp.game.global.map.props.gridSpaceY) * GameApp.game.global.map.props.gridSpaceY);
            }
            else {
                GameApp.game.global.map.marker.p1.x = -200;
                GameApp.game.global.map.marker.p1.y = -200;
            }
        }else{
            console.log('----PLAYER 2 UPDATE------------');
            //console.log()
            if (GameApp.game.global.map.bounds.x(GameApp.game.input.x) && GameApp.game.global.map.bounds.y(GameApp.game.input.y, 2)) {
                /*console.log('--------CALC---------');
                console.log((~~(GameApp.game.input.y / GameApp.game.global.map.props.gridSpaceY) * GameApp.game.global.map.props.gridSpaceY));*/


                GameApp.game.global.map.marker.p2.x = (~~(GameApp.game.input.x / GameApp.game.global.map.props.gridSpaceX) * GameApp.game.global.map.props.gridSpaceX);

                GameApp.game.global.map.marker.p2.y = (Math.floor((GameApp.game.input.y - 30)/ GameApp.game.global.map.props.gridSpaceY) * GameApp.game.global.map.props.gridSpaceY);
                //GameApp.game.global.map.marker.p2.y = (Math.floor((GameApp.game.input.y - 30)/ GameApp.game.global.map.props.gridSpaceY) * GameApp.game.global.map.props.gridSpaceY);
            }
            else {
                GameApp.game.global.map.marker.p2.x = -200;
                GameApp.game.global.map.marker.p2.y = -200;
            }
        }
        /*console.log('update Marker');*/

        //this.marker.x = this.layer2.getTileX(GameApp.game.input.activePointer.worldX) * 32;
        //this.marker.y = this.layer2.getTileY(GameApp.game.input.activePointer.worldY) * 32;

        //if (GameApp.game.input.mousePointer.isDown)
        //{
            //this.tileMap.putTile(this.currentTile, this.currentLayer.getTileX(this.marker.x), this.currentLayer.getTileY(this.marker.y), this.currentLayer);
            // map.fill(currentTile, currentLayer.getTileX(marker.x), currentLayer.getTileY(marker.y), 4, 4, currentLayer);
        //}
    },
    onMouseDown: function() {
        GameApp.game.global['side1']['unitSelected'] = false;
        //GameApp.game.global['side2']['unitSelected'] = false;
        GameApp.game.global['side1']['selectedID'] = 0;
        //GameApp.game.global['side2']['selectedID'] = 0;
        //this.onSelectUpdate();
    },
    pauseMenu: function(){
        // When the paus button is pressed, we pause the game
        GameApp.game.paused = true;

        // Then add the menu
        this.pauseArea = GameApp.game.add.sprite(200, 50, 'menu');
        this.continueGame_menu = GameApp.game.add.text(GameApp.game.width/2, GameApp.game.height-450, 'Continue', { font: '30px Arial', fill: '#FFF' });
        this.continueGame_menu.anchor.setTo(0.5, 0.5);
        this.settings_menu = GameApp.game.add.text(GameApp.game.width/2, GameApp.game.height-400, 'Settings', { font: '30px Arial', fill: '#FFF' });
        this.settings_menu.anchor.setTo(0.5, 0.5);


        // And a label to illustrate which menu item was chosen. (This is not necessary)
        this.choiceLabel = GameApp.game.add.text(GameApp.game.width/2, GameApp.game.height-150, 'Click outside menu to continue', { font: '24px Arial', fill: '#FFF' });
        this.choiceLabel.anchor.setTo(0.5, 0.5);
    },
    resumeGame: function(event){
        // Only act if paused
        if(GameApp.game.paused){
            if(event.x > GameApp.game.global.menu.pause.x1 && event.x < GameApp.game.global.menu.pause.x2 && event.y > GameApp.game.global.menu.pause.y1 && event.y < GameApp.game.global.menu.pause.y2 ){
                console.log('x: ', event.x);
                console.log('y: ', event.y);
            }
            else{
                // Remove the menu and the label
                this.pauseArea.destroy();
                this.continueGame_menu.destroy();
                this.settings_menu.destroy();
                this.choiceLabel.destroy();

                // resume the game
                GameApp.game.paused = false;
            }
        }
    },
    startMenu: function() {
        this.game.state.start('menu');
    },
    gameWin: function() {
        this.game.state.start('menu');
    }
};