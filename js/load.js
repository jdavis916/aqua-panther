var loadState = {
    preload: function () {
        var loadingLabel = GameApp.game.add.text(GameApp.game.world.centerX, 150, 'loading...',
        { font: '30px Arial', fill: '#FFF' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        var progressBar = GameApp.game.add.sprite(GameApp.game.world.centerX, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        GameApp.game.load.setPreloadSprite(progressBar);
        //GameApp.game.load.spritesheet('player', 'assets/player2.png', 20, 20);

        GameApp.game.load.image('background', 'assets/map_01_01.png');
        GameApp.game.load.image('splash', 'assets/splash.jpg');
        GameApp.game.load.image('menu', 'assets/pause_menu.png');
        GameApp.game.load.image('start_menu', 'assets/start_menu.png');
        GameApp.game.load.image('black_screen', 'assets/black_screen.jpg');
        GameApp.game.load.image('captain_1_1_1', 'assets/captain_1_1_1.png');
        GameApp.game.load.image('captain_2_1_1', 'assets/captain_2_1_1.png');

        GameApp.game.load.spritesheet('mute', 'assets/muteButton.png', 28, 22);
        //GameApp.game.load.image('jumpButton', 'assets/jumpButton.png');
        //GameApp.game.load.image('rightButton', 'assets/rightButton.png');
        //GameApp.game.load.image('leftButton', 'assets/leftButton.png');
        //GameApp.game.load.audio('dead', ['assets/dead.ogg', 'assets/dead.mp3']);
        //this.load.image('tileset', 'assets/tileset.png');
        //this.load.tilemap('map', 'assets/map1.json', null, Phaser.Tilemap.TILED_JSON);
        GameApp.game.load.image('ship_1_idle', 'assets/ship_1.png');
        GameApp.game.load.image('ship_1_1_1', 'assets/ship_1_1_1.png');
        GameApp.game.load.image('ship_1_1_2', 'assets/Ship_1_1_2.png');
        GameApp.game.load.spritesheet('ship_2_idle', 'assets/gravityShip_idle.png', 610, 449, 6);

        // json
        GameApp.game.load.json('units', 'data/units.json');
    },
    create: function() {
        // Go to the menu state
        GameApp.game.state.start('lvl_1_1');
    }
};