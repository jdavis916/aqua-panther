var Components = Components || {};

Components.MapBody = Components.MapBody || function() {

    // Reduce conflicts
    var self = this;

    // Name
    self.name = "MapBody";

    // Target
    self.target = null;

    //self.map.side1 = null;
    // -------------------------
    // Properties
    // -------------------------
    //self.physicsEngine = Phaser.Physics.P2JS;

    // -------------------------
    // Events
    // -------------------------
    self.onCreate = new Phaser.Signal();


    // -------------------------
    // Methods
    // -------------------------
    self.setTarget = function(t) {
        self.target = t;
        console.log("[MapBody] :: added target " + self.target);

        //t.game.physics.enable(t, self.physicsEngine);

        self.createMap();
    };


    self.update = function() {
        //console.log("[mapBody] :: update");
    };
    self.render = function() {
        //console.log("[mapBody] :: render");
    };
    self.createMap = function() {
        //console.log("[mapBody] :: map created");
    };
};