var Components = Components || {};

Components.UnitBody = Components.UnitBody || function(props) {

    // Reduce conflicts
    var self = this;
    //console.log(props.type);
    // Name
    self.name = "UnitBody";


    //self.props.unitName = .type;
    // Target
    self.target = null;


    // -------------------------
    // Properties
    // -------------------------
    self.physicsEngine = Phaser.Physics.P2JS;
    self.type = props.type;

    // -------------------------
    // Events
    // -------------------------
    self.onCreate = new Phaser.Signal();


    // -------------------------
    // Methods
    // -------------------------
    self.setTarget = function(t) {
        self.target = t;
        //console.log("[UnitBody] :: added target " + self.name);

        t.game.physics.enable(t, self.physicsEngine);
    };


    self.update = function() {
        //console.log("[UnitBody] :: update");
        //console.log('type : ',self.type);
        //console.log('active : ', self.id);
        //console.log('selected : ', self.target.id);
        if(self.target.id === GameApp.game.global.updates.player['p' + self.target.unitSide].unitID){
            //console.log('active unit : ', self.target.id);
            self.target.unitSelected = 1;
        }else{
            self.target.unitSelected = 0;
        }

        if(self.target.unitSelected === 1){
            //console.log('active : ', self.target.id);
            self.target.x = (~~(GameApp.game.input.mousePointer.x / GameApp.game.global.map.props.gridSpaceX)*GameApp.game.global.map.props.gridSpaceX);
            if(self.target.x < GameApp.game.global.map.props.sideStart){
                self.target.x = GameApp.game.global.map.props.sideStart;
            }else if (self.target.x > GameApp.game.global.map.props.sideEnd - GameApp.game.global.map.props.gridSpaceX){
                self.target.x = GameApp.game.global.map.props.sideEnd - GameApp.game.global.map.props.gridSpaceX;
            }
            //(~~(GameApp.game.input.x / GameApp.game.global.map.props.gridSpaceX)*GameApp.game.global.map.props.gridSpaceX);
        }
    }

};