//"use strict";
var GameApp = GameApp || {};
var dragonBones = dragonBones || {};
GameApp.game = {
    setup : function(msg){
        if(msg === "game start")
            return "game object exists";
    },
    levelManager : {
        level : {
            section : function(level, section){
                if(level === 1 && section === 5)
                {
                    return "Level: " +level+ ":" +section+ " Loaded";                  
                }  
                         
            },
            activeObjects : function(level, section){
                if(level === 1 && section === 2)
                {
                    return "Level: " +level+ ":" +section+ " | Active Objects Loaded";                         
                }                        
            }
        },
        enemies : {
            levelStart : function(level, section){
                var grunts = 0;
                var elites = 0;
                var miniBosses = 0;
                var bosses = 0;
                if(level === 1 && section === 3)
                {
                    grunts = 15;                      
                }      
                return "Level " +level+ ":" +section+ " | Grunts: " +grunts+ "| Elites: " +elites+ "| Mini-Bosses: " 
                    +miniBosses+ "| Bosses: " +bosses+" Loaded";                  
            },

        },
        items : {
            levelStart : function(level, section){
                if(level === 1 && section === 4)
                {
                    return "Level: " +level+ ":" +section+ " | Starting Items Loaded";                  
                }                          
            }
        }
    },
    player : {
        profile : {
            progression : function(chapter, level, section){
                return "Progress: " + chapter + ":" +level+ ":" + section + " Loaded";    
            }   
        }
    }
}
// GameApp.player.setup = function(msg){
//     // if(msg === "game start")

// }