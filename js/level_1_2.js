GameApp.Level_1_2 = function() {
    this.game = GameApp.game;
    this.platforms = null;
    this.stars = null;
    this.cursor = null;
    this.logo1 = null;
    this.land = null;
    this.map = null;
    this.tileset = null;
    this.pathfinder = null;
    this.player = null;
    this.playerState = "none";
    this.playerDirection = "";
    this.wSpeed = 200; // walk speed
    this.rSpeed = 350; // run speed
    this.aSpeed = 200; // in air speed
    this.startY = 480; // starting y position of player per cycle
    this.endY = 0; // ending y position of player per cycle
    this.marker = null;
    this.blocked = null;
    this.bullets = null;
    this.fireRate = 10;
    this.nextFire = 2;
    this.enemiesAlive = 0;
    this.enemies = [];
};

GameApp.Level_1_2.prototype = {

    preload: function() {
        this.game.load.image('sky', 'assets/sky.png');
        this.game.load.spritesheet('player', 'assets/player2.png', 20, 20);
        this.game.load.image('player_duck', 'assets/player_duck.png');
        this.game.load.image('enemy', 'assets/enemy.png');
        this.game.load.image('coin', 'assets/coin.png');
        this.game.load.image('door', 'assets/door.png');
        this.game.load.image('tileset_1', 'assets/techblocks2_1.png');
        this.game.load.tilemap('map', 'assets/test1.json', null, Phaser.Tilemap.TILED_JSON);
    },

    create: function() 
    {
        // enable physics arcade system
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

     //    this.map = this.game.add.tilemap('test');
        
  //    // addTileSetImage(json-name, phaser-name)
     //    this.map.addTilesetImage('Test', 'tiles');
        
     //    // this.currentTile = this.map.getTile(2, 3);

     //    // setBounds(map-start-x, map-start-y, width, height)
        // //  Resize our game world to be a 2000 x 2000 square
        this.game.world.setBounds(-200, -200, 1000, 1000);

     //    // console.log(this.game.world);
     //    this.layer = this.map.createLayer('Ground');

     //    // resizeWorld resets world size to match this layer
  //    this.layer.resizeWorld();
        
        // // add background for this level
        // this.game.add.sprite(0, 0, 'sky');

        // var walkables = [30];

     //    // this.pathfinder = this.game.plugins.add(Phaser.Plugin.PathFinderPlugin);
     //    // this.pathfinder.setGrid(this.map.layers[0].data, walkables);

        this.player = this.game.add.sprite(50, this.startY, 'player');
        // this.player = this.game.add.sprite(50, this.startY, 'mTest');
        this.setState("standing");
        // this.player.mode = 'standing';
        this.player.anchor.setTo(0.5, 0.5);
        this.game.camera.follow(this.player);
        this.game.physics.enable(this.player);
        
        this.game.physics.arcade.TILE_BIAS = 40;
        
        
        this.cursor = this.game.input.keyboard.createCursorKeys();
        this.game.input.keyboard.addKeyCapture([Phaser.Keyboard.UP, Phaser.Keyboard.DOWN, Phaser.Keyboard.LEFT, 
            Phaser.Keyboard.RIGHT, Phaser.Keyboard.SPACEBAR]);
        this.wasd = {
            up: this.game.input.keyboard.addKey(Phaser.Keyboard.W),
            left: this.game.input.keyboard.addKey(Phaser.Keyboard.A),
            down: this.game.input.keyboard.addKey(Phaser.Keyboard.S),
            right: this.game.input.keyboard.addKey(Phaser.Keyboard.D),
            space: this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)            
        };
     //    this.marker = this.game.add.graphics();
     //    // this.marker.lineStyle(2, 0x000000, 1);
     //    // this.marker.drawRect(0, 0, 32, 32);



        
        this.player.body.collideWorldBounds = true;
        this.player.body.gravity.y = 500;
        // Create the 'right' animation by looping the frames 1 and 2
        this.player.animations.add('right', [13, 15, 17], 30, true);
        // Create the 'left' animation by looping the frames 3 and 4
        this.player.animations.add('left', [12, 14, 16], 30, true);

        // this.player.animations.add('duck',  [], 8);
     //    //  Our tiled scrolling background
     //    // this.land = this.game.add.tileSprite(0, 0, 800, 600, 'sky');
     //    // this.land.fixedToCamera = true;
        console.log(this.player);
        this.createWorld();
        this.state_label = this.game.add.text(600, 25, this.playerState, { font: '20px Arial', fill: '#FFF' });
        // this.state_label.inputEnabled = true;
        this.state_label.fixedToCamera = true;
        // If the game is running on a mobile device
        // if (!game.device.desktop) {
        //     // Display the mobile inputs
        //     this.addMobileInputs();
        // }     
        this.door = this.game.add.sprite(2400, 600, 'door');
        this.game.physics.arcade.enable(this.door);
        this.door.body.immovable = true;

        // Create the emitter with 15 particles. We don't need to set the x and y
        // Since we don't know where to do the explosion yet
        this.emitter = this.game.add.emitter(0, 0, 15);
        // Set the 'pixel' image for the particles
        this.emitter.makeParticles('pixel');
        // Set the y speed of the particles between -150 and 150
        // The speed will be randomly picked between -150 and 150 for each particle
        this.emitter.setYSpeed(-150, 150);
        // Do the same for the x speed
        this.emitter.setXSpeed(-150, 150);
        // Use no gravity for the particles
        this.emitter.gravity = 0;
        this.jumpSound = this.game.add.audio('jump');
        this.coinSound = this.game.add.audio('coin');
        this.deadSound = this.game.add.audio('dead');

        this.enemies = this.game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(10, 'enemy');

        // Contains the time of the next enemy creation
        this.nextEnemy = 0;
        // this.pause_label = this.game.add.text(100, 200, 'Pause', { font: '24px Arial', fill: '#000' });
        // this.pause_label.inputEnabled = true;
        // this.pause_label.fixedToCamera = true;
        // this.pause_label.events.onInputUp.add(this.pauseButton);

        //  Our bullet group
        this.bullets = this.game.add.group();
        this.bullets.enableBody = true;
        this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.bullets.createMultiple(300, 'coin', 0, false);
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 0.5);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
     
        
     //     // set the arcade system to ground
        // this.game.physics.arcade.enable(ground);


     //    //  This stops it from falling away when you jump on it
     //    ground.body.immovable = true;

     //    //  Now let's create two ledges
       // var ledge1 = this.platforms.create(400, 400, 'wallH');
     //    // var ledge2 = this.platforms.create(-150, 250, 'ground');

     //    // set the arcade system to the ledges
        // this.game.physics.arcade.enable(ledge1);
        // // this.game.physics.arcade.enable(ledge2);

        // // set ledges to static placement so they don't move
        // ledge1.body.immovable = true;
     //    // ledge2.body.immovable = true;

        

     //    // create a group for stars
     //    this.stars = this.game.add.group();
     
     //    //  Here we'll create 12 of them evenly spaced apart
     //  //   for (var i = 0; i < 12; i++)
     //  //   {
     //  //       //  Create a star inside of the 'stars' group
     //  //       var star = this.stars.create(i * 70, 0, 'star');

     //     //  // set the arcade system to star
        //  // this.game.physics.arcade.enable(star);

     //  //       //  Let gravity do its thing
     //  //       // star.body.gravity.y = 180;
     
     //  //       //  This just gives each star a slightly random bounce value
     //  //       star.body.bounce.y = 0.7 + Math.random() * 0.2;
     //  //   }

     //    this.cursors = this.game.input.keyboard.createCursorKeys();
    },

    update: function() {
        // this.game.physics.arcade.collide(this.stars, this.platforms);
        this.game.physics.arcade.collide(this.player, this.layer);
        this.game.physics.arcade.collide(this.enemies, this.layer);
        this.movePlayerState();
        this.game.physics.arcade.overlap(this.player, this.door, this.playerWin, null, this);

        this.enemyTimer();
        // Call the 'playerDie' function when the player and an enemy overlap
        // this.game.physics.arcade.overlap(this.player, this.enemies, this.playerDie, null, this);

        // this.game.physics.arcade.overlap(this.bullets, this.enemies, this.hitEnemy, null, this);
        // for (var i = 0; i < enemies.length; i++)
        // {
        //     if (enemies[i].alive)
        //     {
        //         this.enemiesAlive++;
        //         this.game.physics.arcade.collide(tank, enemies[i].tank);
        //         this.game.physics.arcade.overlap(bullets, enemies[i].tank, bulletHitEnemy, null, this);
        //         enemies[i].update();
        //     }
        // }
        // this.player.body.velocity.x = 0;
        // this.player.body.velocity.y = 0;
        // this.player.body.angularVelocity = 0;

        // if (this.cursors.left.isDown)
        // {
        //     this.player.body.angularVelocity = -200;
        // }
        // else if (this.cursors.right.isDown)
        // {
        //     this.player.body.angularVelocity = 200;
        // }

        // if (this.cursors.up.isDown)
        // {
        //     this.player.body.velocity.copyFrom(this.game.physics.arcade.velocityFromAngle(this.player.angle, 300));
        // }

        // // this.marker.x = this.layer.getTileX(this.game.input.activePointer.worldX) * 32;
        // // this.marker.y = this.layer.getTileY(this.game.input.activePointer.worldY) * 32;

        // if (this.game.input.mousePointer.isDown)
        // {
        //     blocked = true;
        //     // findPathTo(layer.getTileX(this.marker.x), this.layer.getTileY(this.marker.y));
        // }

        // console.log("offset : "+this.player.body.offset);

        if (this.game.input.activePointer.isDown)
        {
            //  Boom!
            // this.fire();
            // this.shootBullet();
            console.log('fire');
        }
        // if(Phaser.Keyboard.SHIFT) {
        //     console.log(e.keyCode);
        // });
        this.getState();
        this.endY = this.getStartPlayerY();
    },

    render: function() {
        this.game.debug.cameraInfo(this.game.camera, 32, 32);
    },
    createWorld: function() {
        // Create the tilemap
        this.map = this.game.add.tilemap('map');
        // Add the tileset to the map
        this.map.addTilesetImage('tileset_1');
        // Create the layer, by specifying the name of the Tiled layer
        this.layer = this.map.createLayer('Foreground1');
        // Set the world size to match the size of the layer
        this.layer.resizeWorld();
        // Enable collisions for the first element of our tileset (the blue wall)
        for (var i = 0; i < 49; i++) {
            this.map.setCollision(i);
        };
        // this.map.setCollision(19);
    },
    getStartPlayerY: function() {
        return ~~this.player.body.y;
    },    
    getState: function() {
        // console.log(this.playerState);
        this.state_label.text = this.playerState;
    },
    setState: function(state) {
        this.playerState = state;
    },
    movePlayer: function(){


        if ((this.cursor.up.isDown || this.wasd.up.isDown) && this.player.body.onFloor()) 
        { 
            this.__jump();
        }        
        if (!this.player.body.onFloor())
        {
            if(~~this.player.body.y < this.endY)
                this.setState("jumping")
            else
                this.setState("falling");
        }
    },
    movePlayerState: function() {        
        if (this.playerState === "standing") 
        { 
            this.standingState();
        }
        else if (this.playerState === "walking") 
        {
            this.walkingState();
        }
        else if (this.playerState === "running") 
        {
            this.runningState();
        }
        else if (this.playerState === "crouching") 
        {
            this.crouchingState();
        }
        else if (this.playerState === "crawling") 
        { 
            this.crawlingState();
        }else if (this.playerState === "jumping") 
        { 
            this.jumpingState();
        }
        else if (this.playerState === "falling") 
        { 
            if(this.player.body.onFloor())
                this.setState("standing");
            else
                this.fallingState();        
        }else{
            // this.standingState();

        }
        this.movePlayer();
        // if (this.playerState.match(/^(crouching|crawling)$/)) { 
        //     // this.player.animations.play('duck');
        //     // this.player.frame = 5; // Set the player frame to 5 (duck)
        //     // this.setState("crouching");
        //     this.getState();
        // }

    },
    standingState: function() {
        if (this.cursor.left.isDown || this.wasd.left.isDown || this.moveLeft) 
        {                
            this.playerDirection = "left";
            this.stand__walk_run(this.playerDirection);
        }
        else if (this.cursor.right.isDown || this.wasd.right.isDown || this.moveRight) 
        {
            this.playerDirection = "right";
            this.stand__walk_run(this.playerDirection);
        }
        else {
            this.player.body.velocity.x = 0;
            this.player.animations.stop(); // Stop the animation
            this.player.frame = 13; // Set the player frame to 0 (stand still)
            this.playerDirection = "still";
            // this.stand__walk_run(this.playerDirection);
            this.setState("standing");
        }
            // if ((this.cursor.up.isDown || this.wasd.up.isDown) && this.player.body.onFloor()) { 
            //     this.__jump();

            // }
            if ((this.cursor.down.isDown || this.wasd.down.isDown) && this.player.body.onFloor()) { 
                // this.player.animations.play('duck');
                // this.player.frame = 5; // Set the player frame to 5 (duck)
                if(this.wasd.left.isDown || this.wasd.right.isDown)
                {
                    this.setState("crawling");
                }else{
                    this.setState("crouching");
                }    
            }
        this.getState();
    },
    walkingState: function() {
        if (this.cursor.left.isDown || this.wasd.left.isDown || this.moveLeft) 
        {     
            this.stand__walk_run("left");                
        }
        else if (this.cursor.right.isDown || this.wasd.right.isDown || this.moveRight) 
        {
            this.stand__walk_run("right");
        }
        else if(this.wasd.down.isDown)
        {
            this.__crouch();
        }
        else {
            this.__stand();
        }
        this.getState();
    },
    runningState: function() {
        if (this.cursor.left.isDown || this.wasd.left.isDown || this.moveLeft) 
        {     
            this.stand__walk_run("left");                
        }
        else if (this.cursor.right.isDown || this.wasd.right.isDown || this.moveRight) 
        {
            this.stand__walk_run("right");
        }
        else {
           this.__stand();
        }
        this.getState();
    },
    crouchingState: function() {
        if (this.cursor.left.isDown || this.wasd.left.isDown || this.moveLeft) 
        {     
            this.stand__walk_run("left");                
        }
        else if (this.cursor.right.isDown || this.wasd.right.isDown || this.moveRight) 
        {
            this.stand__walk_run("right");
        }
        else {
           this.__stand();
        }
        
        this.getState();
    },
    crawlingState: function() {
        this.getState();
    },
    jumpingState: function() {
        if (this.cursor.left.isDown || this.wasd.left.isDown || this.moveLeft) 
        {                
            this.playerDirection = "left";
            // this.stand__walk_run(this.playerDirection);
            this.__air();
        }
        else if (this.cursor.right.isDown || this.wasd.right.isDown || this.moveRight) 
        {
            this.playerDirection = "right";
            // this.stand__walk_run(this.playerDirection);
            this.__air();
        }
        else {
            this.__still();
        }

        
        this.getState();
    },
    fallingState: function() {
        if (this.cursor.left.isDown || this.wasd.left.isDown || this.moveLeft) 
        {                
            this.playerDirection = "left";
            this.__air();
        }
        else if (this.cursor.right.isDown || this.wasd.right.isDown || this.moveRight) 
        {
            this.playerDirection = "right";
            this.__air();
        }
        else {
            this.__stand();
        }
        this.getState();
    },
    __still: function(){
        this.player.body.velocity.x = 0;
        this.player.animations.stop(); // Stop the animation
        this.player.frame = 0; // Set the player frame to 0 (stand still)
    },
    __stand: function(){
        this.__still();
        this.setState("standing");
    },
    stand__walk_run: function(anim) {
        var dir = 1;
        if(anim !== "right") dir = -1;
        if(this.player.body.onFloor()){
            if(this.wasd.space.isDown){
                speed = (dir)*this.rSpeed;
                this.setState("running"); 
            }                    
            else{
                speed = (dir)*this.wSpeed;
                this.setState("walking");                        
            }
            this.player.body.velocity.x = speed;                
        }else{
            this.player.body.velocity.x = (dir)*this.aSpeed;
        }
        
        this.player.animations.play(anim);
    },
    __air: function() {
        var dir = 1;
        if(this.playerDirection === "left") dir = -1;
        speed = (dir)*this.aSpeed;
        this.player.body.velocity.x = speed;
        this.player.animations.play(this.playerDirection);
    },
    __crouch: function(){
        this.setState("crouching");
    },
    __jump: function() {
        // If the player is touching the ground
        if (this.player.body.onFloor()) 
        {
            // Jump with sound
            this.player.body.velocity.y = -320;
            // this.jumpSound.play();
            this.setState("jumping");
        }
    },
    fire: function() {
        if (this.game.time.now > this.nextFire && this.bullets.countDead() > 0)
        {
            nextFire = this.game.time.now + this.fireRate;

            var bullet = this.bullets.getFirstExists(false);

            bullet.reset(this.player.x + 22, this.player.y - 6);

            bullet.rotation = this.game.physics.arcade.moveToPointer(bullet, 1000, this.game.input.activePointer, 500);
        }
    },
    shootBullet: function() {
        // Enforce a short delay between shots by recording
        // the time that each bullet is shot and testing if
        // the amount of time since the last shot is more than
        // the required delay.
        if (this.lastBulletShotAt === undefined) this.lastBulletShotAt = 0;
        if (this.game.time.now - this.lastBulletShotAt < this.SHOT_DELAY) return;
        this.lastBulletShotAt = this.game.time.now;

        // Get a dead bullet from the pool
        var bullet = this.bulletPool.getFirstDead();

        // If there aren't any bullets available then don't shoot
        if (bullet === null || bullet === undefined) return;

        // Revive the bullet
        // This makes the bullet "alive"
        bullet.revive();

        // Bullets should kill themselves when they leave the world.
        // Phaser takes care of this for me by setting this flag
        // but you can do it yourself by killing the bullet if
        // its x,y coordinates are outside of the world.
        bullet.checkWorldBounds = true;
        bullet.outOfBoundsKill = true;

        // Set the bullet position to the gun position.
        bullet.reset(this.gun.x, this.gun.y);

        // Shoot it
        bullet.body.velocity.x = this.BULLET_SPEED;
        bullet.body.velocity.y = 0;
    },
    addMobileInputs: function() {
        // Add the jump button
        this.jumpButton = game.add.sprite(350, 247, 'jumpButton');
        this.jumpButton.inputEnabled = true;
        this.jumpButton.events.onInputDown.add(this.__jump, this);
        this.jumpButton.alpha = 0.5;
        // Movement variables
        this.moveLeft = false;
        this.moveRight = false;
        // Add the move left button
        this.leftButton = this.game.add.sprite(50, 247, 'leftButton');
        this.leftButton.inputEnabled = true;
        this.leftButton.events.onInputOver.add(function(){this.moveLeft=true;}, this);
        this.leftButton.events.onInputOut.add(function(){this.moveLeft=false;}, this);
        this.leftButton.events.onInputDown.add(function(){this.moveLeft=true;}, this);
        this.leftButton.events.onInputUp.add(function(){this.moveLeft=false;}, this);
        this.leftButton.alpha = 0.5;
        // Add the move right button
        this.rightButton = this.game.add.sprite(130, 247, 'rightButton');
        this.rightButton.inputEnabled = true;
        this.rightButton.events.onInputOver.add(function(){this.moveRight=true;},
        this);
        this.rightButton.events.onInputOut.add(function(){this.moveRight=false;},
        this);
        this.rightButton.events.onInputDown.add(function(){this.moveRight=true;},
        this);
        this.rightButton.events.onInputUp.add(function(){this.moveRight=false;},
        this);
        this.rightButton.alpha = 0.5;
    },
    playerDie: function() {
        if (!this.player.alive) {
            return;
        }
        this.player.kill();
        this.deadSound.play();
        // Set the position of the emitter on the player
        this.emitter.x = this.player.x;
        this.emitter.y = this.player.y;
        // Start the emitter, by exploding 15 particles that will live for 600ms
        this.emitter.start(true, 600, null, 15);
        // Call the 'startMenu' function in 1000ms
        this.game.time.events.add(1000, this.startMenu, this);
    },
    playerWin: function() {
        // if (!this.player.alive) {
        //     return;
        // }
        // this.player.kill();
        // this.deadSound.play();
        // Set the position of the emitter on the player
        // this.emitter.x = this.player.x;
        // this.emitter.y = this.player.y;
        // Start the emitter, by exploding 15 particles that will live for 600ms
        // this.emitter.start(true, 600, null, 15);
        // Call the 'startMenu' function in 1000ms
        this.game.time.events.add(100, this.startMenu, this);
    },
    startMenu: function() {
        this.game.state.start('menu');
    },
    pauseButton: function() {
        console.log(this.game.paused);
        if(!this.game.paused)
        {
            this.game.paused = true;
            // Then add the menu
            this.menu = this.game.add.sprite(w/2, h/2, 'coin');
            this.menu.anchor.setTo(0.5, 0.5);
            this.menu.events.onInputUp.add(this.pauseButton);
        }            

        if(this.game.paused){
            var w = 800, h = 600;
            // Calculate the corners of the menu
            var x1 = w/2 - 270/2, x2 = w/2 + 270/2,
                y1 = h/2 - 180/2, y2 = h/2 + 180/2;

            // Check if the click was inside the menu
            if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
                // The choicemap is an array that will help us see which item was clicked
                // var choisemap = ['one', 'two', 'three', 'four', 'five', 'six'];

                // Get menu local coordinates for the click
                var x = event.x - x1,
                    y = event.y - y1;

                // Calculate the choice 
                // var choise = Math.floor(x / 90) + 3*Math.floor(y / 90);

                // Display the choice
                // choiseLabel.text = 'You chose menu item: ' + choisemap[choise];
            }
            else{
                // Remove the menu and the label
                menu.destroy();
                // choiseLabel.destroy();

                // Unpause the game
                this.game.paused = false;
            }
        }

        
    },
    pauseMenu: function() {
        // if(game.paused){
        //     // Calculate the corners of the menu
        //     var x1 = w/2 - 270/2, x2 = w/2 + 270/2,
        //         y1 = h/2 - 180/2, y2 = h/2 + 180/2;

        //     // Check if the click was inside the menu
        //     if(event.x > x1 && event.x < x2 && event.y > y1 && event.y < y2 ){
        //         // The choicemap is an array that will help us see which item was clicked
        //         var choisemap = ['one', 'two', 'three', 'four', 'five', 'six'];

        //         // Get menu local coordinates for the click
        //         var x = event.x - x1,
        //             y = event.y - y1;

        //         // Calculate the choice 
        //         var choise = Math.floor(x / 90) + 3*Math.floor(y / 90);

        //         // Display the choice
        //         choiseLabel.text = 'You chose menu item: ' + choisemap[choise];
        //     }
        //     else{
        //         // Remove the menu and the label
        //         menu.destroy();
        //         choiseLabel.destroy();

        //         // Unpause the game
        //         game.paused = false;
        //     }
        // }
    },
    gameWin: function() {
        this.game.state.start('menu');
    },
    enemyTimer: function() {
        // If the 'nextEnemy' time has passed
        if (this.nextEnemy < this.game.time.now) {
            // Define our variables
            var start = 1000, end = 1000, score = 100;
            // Formula to decrease the delay between enemies over time
            // At first it's 4000ms, then slowly goes to 1000ms
            var delay = Math.max(start - (start-end)*this.game.global.score/score, end);
            // console.log("delay: "+delay);
            // Create a new enemy, and update the 'nextEnemy' time
            this.addEnemy();
            this.nextEnemy = this.game.time.now + delay;
        }
    },
    addEnemy: function() {
        // Get the first dead enemy of the group
        var enemy = this.enemies.getFirstDead();
        // If there isn't any dead enemy, do nothing
        if (!enemy) {
            return;
        }
        // Initialise the enemy
        enemy.anchor.setTo(0.5, 1);
        // enemy.reset(game.world.centerX, 0);
        enemy.reset(this.randomNum(300, 1800), 0);
        enemy.body.gravity.y = 500;
        enemy.body.velocity.x = 100 * Phaser.Math.randomSign();
        enemy.body.bounce.x = 1;
        enemy.body.collideWorldBounds = true;
        enemy.checkWorldBounds = true;
        enemy.outOfBoundsKill = true;
    },
    hitEnemy: function(_bullet, _enemy) {
        console.log("enemy hit");
        _bullet.kill();
        _enemy.kill();
    },
    randomNum: function(min,max) {
        return Math.floor(Math.random()*(max-min+1)+min);
    }
};