QUnit.test("Game created", function( assert ) 
{
    function test(msg, expected) {
        assert.equal(GameApp.game.setup(msg), expected);
    }
    test("game start", "game object exists");
});

// QUnit.test("Player Loaded", function( assert ) 
// {
//     function test(msg, expected) {
//         assert.equal(GameApp.game.player.profile(msg), expected);
//     }
//     test("load player", "player object Loaded");
// });

QUnit.test("Player Loaded", function( assert ) 
{
    function progression(chapter, level, section, expected) {
        assert.equal(GameApp.game.player.profile.progression(chapter, level, section), expected);
    }
    progression(1, 1, 1, "Progress: 1:1:1 Loaded");
    progression(2, 3, 3, "Progress: 2:3:3 Loaded");
});
// QUnit.test("Story Progress Loaded", function( assert ) 
// {
//     function test(msg, expected) {
//         assert.equal(GameApp.game.progress(msg), expected);
//     }
//     test("load maps", "map object Loaded");
// });

// QUnit.test("Levels Loaded", function( assert ) 
// {
//     function test(msg, expected) {
//         assert.equal(GameApp.game.levelManager(msg), expected);
//     }
//     test("load maps", "map object Loaded")
// });

QUnit.test("Current Level Loaded", function( assert ) 
{
    function level(level, section, expected) {
        assert.equal(GameApp.game.levelManager.level.section(level, section), expected);
    }
    function enemies(level, section, expected) {
        assert.equal(GameApp.game.levelManager.enemies.levelStart(level, section), expected);
    }
    function activeObjects(level, section, expected) {
        assert.equal(GameApp.game.levelManager.level.activeObjects(level, section), expected);
    }
    function items(level, section, expected) {
        assert.equal(GameApp.game.levelManager.items.levelStart(level, section), expected);
    }
    level(1, 5, "Level: 1:5 Loaded");
    activeObjects(1, 2, "Level: 1:2 | Active Objects Loaded");
    enemies(1, 3, "Level 1:3 | Grunts: 15| Elites: 0| Mini-Bosses: 0| Bosses: 0 Loaded");
    items(1, 4, "Level: 1:4 | Starting Items Loaded");

});



// QUnit.test("Player Loaded", function( assert ) 
// {
//     function playerObj(msg, expected) {
//         assert.equal(GameApp.player(msg), expected);
//     }
//     playerObj("load player", "player object exists")
// });
// QUnit.test("Access player object", function( assert ) 
// {
//     function playerObj(then, expected) {
//         assert.equal(GameApp.player.setup("game Loaded"), expected);
//     }
//     playerObj("game start", "player exists")
// });

// function domtest(name, now, first, second) 
// {
//     QUnit.test(name, function( assert ) 
//     {
//         var links = document.getElementById("qunit-fixture").getElementsByTagName("a");
//         assert.equal(links[0].innerHTML, "January 28th, 2008");
//         assert.equal(links[2].innerHTML, "January 27th, 2008");
//         prettyDate.update(now);
//         assert.equal(links[0].innerHTML, first);
//         assert.equal(links[2].innerHTML, second);
//     });
// }
// domtest("prettyDate.update", "2008-01-28T22:25:00Z", "2 hours ago", "Yesterday");
// domtest("prettyDate.update, one day later", "2008/01/29 22:25:00", "Yesterday", "2 days ago");